'use strict';

const Sequelize = require('sequelize');
const path = require('path');
const config = require(path.join(process.cwd(), 'config', 'database.js'));

module.exports = {
  development: {
    username: config.default.database().username,
    password: config.default.database().password,
    database: config.default.database().db,
    host: config.default.database().options.host,
    port: config.default.database().options.port,
    dialect: config.default.database().options.dialect,
    seederStorage: 'sequelize',
    define: {
      freezeTableName: true
    }
  },

  test: {
    username: config.test.database().username,
    password: config.test.database().password,
    database: config.test.database().db,
    host: config.test.database().options.host,
    port: config.test.database().options.port,
    dialect: config.test.database().options.dialect,
    seederStorage: 'sequelize',
    define: {
      freezeTableName: true
    }
  },

  production: {
    username: config.default.database().username,
    password: config.default.database().password,
    database: config.default.database().db,
    host: config.default.database().options.host,
    port: config.default.database().options.port,
    dialect: config.default.database().options.dialect,
    seederStorage: 'sequelize',
    define: {
      freezeTableName: true
    }
  }
};
