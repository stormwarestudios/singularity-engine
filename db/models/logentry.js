'use strict';
module.exports = (sequelize, DataTypes) => {
  const LogEntry = sequelize.define(
    'LogEntry',
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4
      },
      organizationOwnerId: {
        type: DataTypes.UUID,
        defaultValue: null
      },
      ownerId: {
        type: DataTypes.UUID,
        defaultValue: null
      },
      sourceEntityId: {
        type: DataTypes.UUID,
        defaultValue: null
      },
      targetEntityId: {
        type: DataTypes.UUID,
        defaultValue: null
      },
      text: {
        type: DataTypes.TEXT
      },
      recordedAt: {
        type: DataTypes.DATE,
        defaultValue: new Date()
      },
      tags: {
        type: DataTypes.JSONB
      },
      toBeSynchronized: {
        type: DataTypes.BOOLEAN
      },
      synchronizedAt: {
        type: DataTypes.DATE
      },
      hash: {
        type: DataTypes.TEXT,
        defaultValue: null
      }
    },
    {}
  );
  LogEntry.associate = function(models) {
    // associations can be defined here
  };
  return LogEntry;
};
