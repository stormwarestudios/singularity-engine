'use strict';
module.exports = (sequelize, DataTypes) => {
  const ServerNodeRole = sequelize.define('ServerNodeRole', {
    name: DataTypes.STRING
  }, {});
  ServerNodeRole.associate = function(models) {
    // associations can be defined here
    ServerNodeRole.hasMany(models.ServerNode, { foreignKey: 'serverNodeRoleId' });
  };
  return ServerNodeRole;
};