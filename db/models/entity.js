'use strict';

const { api } = require('../..');

module.exports = (sequelize, DataTypes) => {
  const Entity = sequelize.define(
    'Entity',
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4
      },
      data: {
        type: DataTypes.JSONB,
        defaultValue: {}
      }
    },
    {}
  );
  Entity.associate = function(models) {
    // associations can be defined here
  };

  Entity.getByComponentValue = function(component, key, value) {
    const sql = `SELECT * FROM "Entity" WHERE data->'_components'->'${component}'->>'${key}' = '${value}'`;
    const type = sequelize.QueryTypes.SELECT;
    return sequelize
      .query(sql, {
        type,
        raw: true,
        logging: (query) => {
          api.log(query, 'debug', null);
        }
      })
      .then(([results, metadata]) => {
        return results;
      });
  };

  return Entity;
};
