'use strict';
module.exports = (sequelize, DataTypes) => {
  const ServerNode = sequelize.define(
    'ServerNode',
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4
      },
      ip: DataTypes.STRING,
      httpPort: DataTypes.INTEGER,
      socketPort: DataTypes.INTEGER,
      serverNodeRoleId: DataTypes.INTEGER
    },
    {}
  );
  ServerNode.associate = function(models) {
    // associations can be defined here
    ServerNode.belongsTo(models.ServerNodeRole, { foreignKey: 'serverNodeRoleId' });
  };
  return ServerNode;
};
