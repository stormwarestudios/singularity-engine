'use strict';

const uuid = require('uuid');
const argon2 = require('argon2');
const config = require('../../config/app').default;

module.exports = {
  up: (queryInterface, Sequelize) => {
    const id = uuid.v4();
    const emailAddress = 'testaccount@galactikore.com';
    const password = 'Pass1word?';
    const verificationCode = null;
    return argon2.hash(password, config.argon).then((password) => {
      return queryInterface.bulkInsert(
        'Account',
        [
          {
            id,
            emailAddress,
            password,
            verificationCode
          }
        ],
        {}
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    const emailAddress = 'testaccount@galactikore.com';
    return queryInterface.bulkDelete(
      'Account',
      {
        emailAddress
      },
      {}
    );
  }
};
