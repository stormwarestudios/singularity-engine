'use strict';

const { Op } = require('sequelize');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('ServerNodeRole', [{ id: 1, name: 'None' }, { id: 2, name: 'Orchestrator' }, { id: 3, name: 'Gateway' }, { id: 4, name: 'GameWorker' }]);
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */

    return queryInterface.bulkDelete('ServerNodeRole', { id: { [Op.in]: [1, 2, 3, 4] } }, {});
  }
};
