'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ServerNode', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      ip: {
        type: Sequelize.STRING
      },
      httpPort: {
        type: Sequelize.INTEGER
      },
      socketPort: {
        type: Sequelize.INTEGER
      },
      serverNodeRoleId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'ServerNodeRole', key: 'id' }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ServerNode');
  }
};
