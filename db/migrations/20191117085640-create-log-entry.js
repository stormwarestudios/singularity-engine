'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'LogEntry',
      {
        id: {
          type: Sequelize.UUID,
          primaryKey: true,
          allowNull: false
        },
        organizationOwnerId: {
          type: Sequelize.UUID,
          allowNull: true
        },
        ownerId: {
          type: Sequelize.UUID,
          allowNull: true
        },
        sourceEntityId: {
          type: Sequelize.UUID,
          allowNull: true
        },
        targetEntityId: {
          type: Sequelize.UUID,
          allowNull: true
        },
        text: {
          type: Sequelize.STRING,
          allowNull: false
        },
        recordedAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        tags: {
          type: Sequelize.JSONB,
          defaultValue: null
        },
        toBeSynchronized: {
          type: Sequelize.BOOLEAN,
          defaultValue: true
        },
        synchronizedAt: {
          allowNull: true,
          type: Sequelize.DATE,
          defaultValue: null
        },
        hash: {
          allowNull: true,
          type: Sequelize.STRING,
          defaultValue: null
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        }
      },
      {
        timestamps: true,
        freezeTableName: true
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('LogEntry');
  }
};
