'use strict';

const jwt = require('jsonwebtoken');
const { api, Process } = require('../..');

module.exports = class WebsocketConnectionMiddleware extends Process {
  constructor() {
    super();

    /**
     * Process name; must be unique in the system.
     * @type {string}
     */
    this.name = 'websocketConnectionMiddleware';

    /**
     * A friendly description for your process.
     * @type {string}
     */
    this.description = 'Websocket Connection Middleware';

    /**
     * Startup and shutdown priority; executed in ascending order on startup, and descending order on shutdown.
     * @type {number}
     */
    this.priority = 1000;

    /**
     * Hook-types to which this process will be configured to respond.
     * @type {Array} Zero or more of connection, command or initializer
     */
    this.hooks = ['connection'];

    /**
     * Connection-types to which this process will be configured to respond.
     * @type {Array} Zero or more of web, websocket, grpc or task
     */
    this.connectionTypes = ['websocket'];
  }

  /**
   * Called as a middleware function, either immediately before exec(), or on connection
   * @param data
   * @returns {Promise<void>}
   */
  async pre(data) {
    await super.pre(data);
    // My preprocess hook code goes here
    api.log('Cookies:', 'debug', data.connection.cookies);

    const {
      connection: {
        cookies: {
          JWT
        }
      }
    } = data;

    if (JWT) {
      const secret = await api.secrets.getSecret('jwt');
      const result = jwt.verify(JWT, secret);

      api.log(`WebSocket JWT authentication: `, 'warn', result);

      const { accountId } = result;
      data.connection.metadata = { accountId };
      data.connection.isAuthenticated = true;
    } else {
      data.connection.disconnect({
        messageType: api.spec.MessageType.NOT_AUTHORIZED,
        message: 'you are not authorized to send messages and will be disconnected'
      });
    }
  }

  /**
   * Called as a middleware function, either immediately after exec(), or on disconnection
   * @param data
   * @returns {Promise<void>}
   */
  async post(data) {
    await super.post(data);
    // My postprocess hook code goes here
  }
};
