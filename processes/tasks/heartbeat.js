'use strict';

const { api, Process } = require('../..');

module.exports = class Heartbeat extends Process {
  constructor() {
    super();

    /**
     * Process name; must be unique in the system.
     * @type {string}
     */
    this.name = 'heartbeat';

    /**
     * A friendly description for your process.
     * @type {string}
     */
    this.description = 'to provide a heartbeat';

    /**
     * A cron-like schedule on which your process will run if scheduled, or null if it should never be scheduled.
     * @type {string|null}
     */
    // this.schedule = '* * * * * *';
    this.schedule = null;

    /**
     * Startup and shutdown priority; executed in ascending order on startup, and descending order on shutdown.
     * @type {number}
     */
    this.priority = 1000;

    /**
     * Hook-types to which this process will be configured to respond.
     * @type {Array} Zero or more of connection, command or initializer
     */
    this.hooks = [];

    /**
     * Connection-types to which this process will be configured to respond.
     * @type {Array} Zero or more of web, websocket, grpc or task
     */
    this.connectionTypes = ['task'];
  }

  /**
   * Called once on startup, in priority order with other processes.
   * @returns {Promise<void>}
   */
  async initialize() {
    await super.initialize();
    // My initialization code goes here
  }

  /**
   * Called as a middleware function, either immediately before exec(), or on connection
   * @param data
   * @returns {Promise<void>}
   */
  async pre(data) {
    await super.pre(data);
    // My preprocess hook code goes here
  }

  /**
   * Called as the main execution for this process.
   * @param {object} data
   * @returns {Promise<void>}
   */
  async exec(data) {
    await super.exec(data);
    // My logic goes here
    /*await api.redis.publish(api.config.application.cluster, api.spec.toBuffer({
      messageType: api.spec.MessageType.WELCOME,
      message: '❤'
    }));*/
  }

  /**
   * Called as a middleware function, either immediately after exec(), or on disconnection
   * @param data
   * @returns {Promise<void>}
   */
  async post(data) {
    await super.post(data);
    // My postprocess hook code goes here
  }

  /**
   * Called once on shutdown, in reverse priority order with other processes.
   * @returns {Promise<void>}
   */
  async shutdown() {
    await super.shutdown();
    // My shutdown code goes here
  }
};
