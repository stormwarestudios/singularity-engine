'use strict';

const { api, Process } = require('../..');
const jwt = require('jsonwebtoken');
const argon2 = require('argon2');

module.exports = class AuthLogin extends Process {
  constructor() {
    super();

    /**
     * Process name; must be unique in the system.
     * @type {string}
     */
    this.name = 'auth:login';

    /**
     * A friendly description for your process.
     * @type {string}
     */
    this.description = 'Login to an existing player account with email address and password';

    /**
     * A cron-like schedule on which your process will run if scheduled, or null if it should never be scheduled.
     * @type {string|null}
     */
    this.schedule = null;

    /**
     * Startup and shutdown priority; executed in ascending order on startup, and descending order on shutdown.
     * @type {number}
     */
    this.priority = 1000;

    /**
     * Hook-types to which this process will be configured to respond.
     * @type {Array} Zero or more of connection, command or initializer
     */
    this.hooks = [];

    /**
     * Connection-types to which this process will be configured to respond.
     * @type {Array} Zero or more of web, websocket, grpc or task
     */
    this.connectionTypes = ['web'];

    /**
     * The web route to this process; only used if this.connectionTypes contains 'web'.
     * @type {null}
     */
    this.route = {
      method: 'post',
      path: '/auth/login'
    };
  }

  /**
   * Called once on startup, in priority order with other processes.
   * @returns {Promise<void>}
   */
  async initialize() {
    await super.initialize();
    // My initialization code goes here
  }

  /**
   * Called as a middleware function, either immediately before exec(), or on connection
   * @param data
   * @returns {Promise<void>}
   */
  async pre(data) {
    await super.pre(data);
    // My preprocess hook code goes here
  }

  /**
   * Called as the main execution for this process.
   * @param {object} data
   * @returns {Promise<void>}
   */
  async exec(data) {
    await super.exec(data);
    // My logic goes here

    const { connection: {
      params: {
        emailAddress,
        password
      }
    } } = data;

    if (!emailAddress) {
      data.connection.rawConnection.res.statusCode = 403;
      data.response = { message: 'invalid username or password' };
      return;
    }

    if (!password) {
      data.connection.rawConnection.res.statusCode = 403;
      data.response = { message: 'invalid username or password' };
      return;
    }

    api.log(`User logging in with email address '${emailAddress}'`, 'info');

    const { db: {
      models: {
        Account
      }
    } } = api.database;

    const account = await Account.findOne({
      where: { emailAddress },
      raw: true
    });

    if (!account) {
      data.connection.rawConnection.res.statusCode = 403;
      data.response = { message: 'invalid username or password' };
      return;
    }

    if (!await argon2.verify(account.password, password)) {
      data.connection.rawConnection.res.statusCode = 403;
      data.response = { message: 'invalid username or password' };
      return;
    }

    const secret = await api.secrets.getSecret('jwt');

    // create a JWT
    const token = jwt.sign(
      {
        accountId: account.id,
        emailAddress
      },
      secret
    );

    api.log(`User logged in, JWT = ${token}`, 'debug', null);

    // See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie

    data.connection.cookies.set('JWT', token, {
      // maxAge: Date.now() + 1000 * 60 * 60 * 24, // 1 day
      // expires: null, // session
      // path: '/',
      // domain: null,
      secure: false,
      httpOnly: true,
      sameSite: false,
      signed: false,
      overwrite: true
    });
  }

  /**
   * Called as a middleware function, either immediately after exec(), or on disconnection
   * @param data
   * @returns {Promise<void>}
   */
  async post(data) {
    await super.post(data);
    // My postprocess hook code goes here
  }

  /**
   * Called once on shutdown, in reverse priority order with other processes.
   * @returns {Promise<void>}
   */
  async shutdown() {
    await super.shutdown();
    // My shutdown code goes here
  }
};
