'use strict';

const { api, Process } = require('../..');

module.exports = class AuthSignup extends Process {
  constructor() {
    super();

    /**
     * Process name; must be unique in the system.
     * @type {string}
     */
    this.name = 'auth:signup';

    /**
     * A friendly description for your process.
     * @type {string}
     */
    this.description = 'Signup for a new player account';

    /**
     * A cron-like schedule on which your process will run if scheduled, or null if it should never be scheduled.
     * @type {string|null}
     */
    this.schedule = null;

    /**
     * Startup and shutdown priority; executed in ascending order on startup, and descending order on shutdown.
     * @type {number}
     */
    this.priority = 1000;

    /**
     * Hook-types to which this process will be configured to respond.
     * @type {Array} Zero or more of connection, command or initializer
     */
    this.hooks = [];

    /**
     * Connection-types to which this process will be configured to respond.
     * @type {Array} Zero or more of web, websocket, grpc or task
     */
    this.connectionTypes = ['web'];

    /**
     * The web route to this process; only used if this.connectionTypes contains 'web'.
     * @type {null}
     */
    this.route = {
      method: 'post',
      path: '/auth/signup'
    };
  }

  /**
   * Called once on startup, in priority order with other processes.
   * @returns {Promise<void>}
   */
  async initialize() {
    await super.initialize();
    // My initialization code goes here
  }

  /**
   * Called as a middleware function, either immediately before exec(), or on connection
   * @param data
   * @returns {Promise<void>}
   */
  async pre(data) {
    await super.pre(data);
    // My preprocess hook code goes here
  }

  /**
   * Called as the main execution for this process.
   * @param {object} data
   * @returns {Promise<void>}
   */
  async exec(data) {
    await super.exec(data);
    // My logic goes here

    const { emailAddress } = data.connection.params;
    const { sendgrid: {
      from
    }} = api.config;

    const { db: {
      models: {
        Account
      }
    } } = api.database;

    // select the account by email address
    const account = await Account.findOne({
      where: {
        emailAddress
      }
    });

    // if it already exists, return error
    if (account) {
      data.connection.rawConnection.res.statusCode = 400;
      api.log(`Account signup for ${emailAddress} failed; account already exists.`, 'warn', null);
      return;
    }

    // Randomly generate a verification code and password
    const verificationCode = await api.engine.generateId(8);
    const password = await api.engine.generateId(32);

    // create a new record, random email token
    await Account.create({
      emailAddress,
      password,
      verificationCode
    });

    const msg = {
      to: emailAddress,
      from: from,
      subject: 'Account Created',
      text: `New account created for ${emailAddress}; use code ${verificationCode} to verify`,
      html: `<p>New account created for ${emailAddress}; use code ${verificationCode} to verify</p>`
    };

    // schedule signup email for sending
    await api.scheduler.enqueue('email:send', msg);

    api.log(`Account signup for ${emailAddress} succeeded; account created and welcome email scheduled for sending.`, 'info');

    data.connection.rawConnection.res.setHeader('Location', [`http://localhost:8080/verify.html`]);
    data.connection.rawConnection.res.statusCode = 201;

    data.response.ok = true;
  }

  /**
   * Called as a middleware function, either immediately after exec(), or on disconnection
   * @param data
   * @returns {Promise<void>}
   */
  async post(data) {
    await super.post(data);
    // My postprocess hook code goes here
  }

  /**
   * Called once on shutdown, in reverse priority order with other processes.
   * @returns {Promise<void>}
   */
  async shutdown() {
    await super.shutdown();
    // My shutdown code goes here
  }
};
