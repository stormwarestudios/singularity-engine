'use strict';

const { api, Process } = require('..');
const osu = require('node-os-utils');

let status = {};

const updateStatus = async () => {
  return {
    cpu: {
      usage: await osu.cpu.usage(),
      // average: osu.cpu.average(),
      free: await osu.cpu.free(),
      count: osu.cpu.count(),
      model: osu.cpu.model(),
    },
    mem: await osu.mem.info(),
    net: {
      inOut: (await osu.netstat.inOut(1000)).total
    },
    os: {
      platform: osu.os.platform(),
      uptime: osu.os.uptime(),
      // ip: osu.os.ip(),
      hostname: osu.os.hostname(),
      type: osu.os.type(),
      arch: osu.os.arch()
    }
  };
};

setInterval(() => {
}, 5000);

module.exports = class MyProcess extends Process {
  constructor() {
    super();

    /**
     * Process name; must be unique in the system.
     * @type {string}
     */
    this.name = 'my process name';

    /**
     * A friendly description for your process.
     * @type {string}
     */
    this.description = 'my process description';

    /**
     * A cron-like schedule on which your process will run if scheduled, or null if it should never be scheduled.
     * @type {string|null}
     */
    this.schedule = null;

    /**
     * Startup and shutdown priority; executed in ascending order on startup, and descending order on shutdown.
     * @type {number}
     */
    this.priority = 1000;

    /**
     * Hook-types to which this process will be configured to respond.
     * @type {Array} Zero or more of connection, command or initializer
     */
    this.hooks = [];

    /**
     * Connection-types to which this process will be configured to respond.
     * @type {Array} Zero or more of web, websocket, grpc or task
     */
    this.connectionTypes = ['web'];

    /**
     * The web route to this process; only used if this.connectionTypes contains 'web'.
     * @type {null}
     */
    this.route = {
      method: 'get',
      path: '/status'
    };
  }

  /**
   * Called once on startup, in priority order with other processes.
   * @returns {Promise<void>}
   */
  async initialize() {
    await super.initialize();
    // My initialization code goes here
  }

  /**
   * Called as a middleware function, either immediately before exec(), or on connection
   * @param data
   * @returns {Promise<void>}
   */
  async pre(data) {
    await super.pre(data);
    // My preprocess hook code goes here
  }

  /**
   * Called as the main execution for this process.
   * @param {object} data
   * @returns {Promise<void>}
   */
  async exec(data) {
    await super.exec(data);
    // My logic goes here

    const promises = [
      osu.cpu.usage(),
      osu.cpu.free(),
      osu.mem.info(),
      osu.netstat.inOut(250)
    ];

    return Promise.all(promises).then(result => {
      const [cpuAverage, cpuFree, mem, netInOutTotal ] = result;
      data.response = {
        cpu: {
          usage: cpuAverage,
          // average: osu.cpu.average(),
          free: cpuFree,
          count: osu.cpu.count(),
          model: osu.cpu.model(),
        },
        mem,
        net: {
          inOut: netInOutTotal
        },
        os: {
          platform: osu.os.platform(),
          uptime: osu.os.uptime(),
          // ip: osu.os.ip(),
          hostname: osu.os.hostname(),
          type: osu.os.type(),
          arch: osu.os.arch()
        }
      };
    });
  }

  /**
   * Called as a middleware function, either immediately after exec(), or on disconnection
   * @param data
   * @returns {Promise<void>}
   */
  async post(data) {
    await super.post(data);
    // My postprocess hook code goes here
  }

  /**
   * Called once on shutdown, in reverse priority order with other processes.
   * @returns {Promise<void>}
   */
  async shutdown() {
    await super.shutdown();
    // My shutdown code goes here
  }
};
