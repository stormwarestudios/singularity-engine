/* eslint-disable security/detect-object-injection */
/* eslint-disable security/detect-non-literal-require */
/* eslint-disable security/detect-non-literal-fs-filename */

'use strict';

const path = require('path');

[
  { className: 'Connection', file: 'connection.js' },
  { className: 'Database', file: 'database.js' },
  { className: 'Engine', file: 'engine.js' },
  // { className: 'Fsm', file: 'fsm.js' },
  // { className: 'GameState', file: 'gamestate.js' },
  { className: 'Logger', file: 'logger.js' },
  // { className: 'Map', file: 'map.js' },
  { className: 'Player', file: 'player.js' },
  { className: 'Process', file: 'process.js' },
  { className: 'Redis', file: 'redis.js' },
  { className: 'Scheduler', file: 'scheduler.js' },
  { className: 'Server', file: 'server.js' },
  { className: 'Spec', file: 'spec.js' }
].forEach(({ className, file }) => {
  const fullPath = path.join(__dirname, 'lib', 'core', file);
  module.exports[className] = require(fullPath);
});

[{ className: 'Secrets', file: 'secrets.js' }].forEach(({ className, file }) => {
  const fullPath = path.join(__dirname, 'lib', 'crypto', file);
  module.exports[className] = require(fullPath);
});

module.exports.api = {
  Engine: require('./lib/core/engine')
};
