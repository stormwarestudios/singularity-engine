'use strict';

const { api, Engine } = require('../..');

module.exports.start = async () => {
  try {
    api.engine = new Engine();
    await api.engine.initialize();
    await api.engine.start();
  } catch (e) {
    console.trace(e);
  }
};
