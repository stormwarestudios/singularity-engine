'use strict';

const fs = require('fs-extra');
const path = require('path');

module.exports.generate = async () => {
  try {
    const directories = {
      config: ['default.json'],
      models: [],
      processes: ['status.js'],
      proto: ['all.proto', 'client.proto', 'common.proto', 'server.proto'],
      public: ['favicon.ico', 'index.html', 'script.js', 'ws.min.js']
    };

    console.log(`Creating project structure:`);
    for (const [dir, files] of Object.entries(directories)) {
      await fs.ensureDir(path.join(process.cwd(), dir));
      console.log(`\t./${dir}`);
      for (const file of files) {
        const src = path.join(__dirname, '..', '..', dir, file);
        const target = path.join(process.cwd(), dir, file);
        await fs.copy(src, target);
        console.log(`\t\t${file}`);
      }
    }
  } catch (e) {
    console.trace(e);
  }
};
