'use strict';

const { api, Server } = require('..');

class SocketServer extends Server {
  constructor() {
    super();
    api.servers = api.servers || {};
    api.servers.socket = this;
    this.type = 'socket';
  }
}

module.exports = SocketServer;
