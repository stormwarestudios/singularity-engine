'use strict';

const { api, Server, Connection } = require('..');
const fs = require('fs');
const path = require('path');
const Primus = require('primus');
const BrowserFingerprint = require('browser_fingerprint');

class WebsocketServer extends Server {
  constructor() {
    super();
    api.servers = api.servers || {};
    api.servers.websocket = this;
    this.primus = null;
    this.type = 'websocket';
    this.spec = null;
    this.connections = {};
    this.fingerprinter = new BrowserFingerprint({
      cookieKey: 'fingerprint',
      toSetCookie: true,
      onlyStaticElements: true,
      settings: {
        path: '/',
        expires: 3600000,
        httpOnly: null
      }
    });
  }

  async initialize() {
    await super.initialize();

    const { options } = api.config.servers.websocket;
    options.idGenerator = api.engine.generateIdSync;

    this.server = new Primus(api.servers.web.server, options);

    api.log(`Accepting websocket connections on ${options.pathname}`);

    if (api.config.servers.websocket.generateClientOnBoot === true) {
      const Terser = require('terser');
      const clientLibrary = this.server.library();
      const minified = Terser.minify(clientLibrary);
      const writeStream = fs.createWriteStream(path.join(process.cwd(), 'public', 'ws.min.js'));
      writeStream.write(minified.code, 'utf8');
    }

    this.server.on('connection', this.onConnection.bind(this));
    this.server.on('disconnection', this.onDisconnection.bind(this));
  }

  async onConnection(client) {
    // let { fingerprint } = this.fingerprinter.fingerprint(req);

    const { /*headers, address, query, socket,*/ id /*request, write*/ } = client;

    const connection = new Connection(id, {
      connectedAt: new Date(),
      remoteIP: client.address.ip,
      remotePort: client.address.port,
      rawConnection: client,
      //fingerprint,
      params: {},
      type: this.type
    });

    api.log(`Client ${id} connected from ${connection.remoteIP}:${connection.remotePort}`);

    for (let i = 0; i < this.middleware.length; i++) {
      // eslint-disable-next-line security/detect-object-injection
      const middleware = this.middleware[i];
      await middleware.pre({ connection });
    }

    // eslint-disable-next-line security/detect-object-injection
    connection.moveToServerWorker(api.config.application.serverWorkers.defaultWorker);

    // eslint-disable-next-line security/detect-object-injection
    connection.send({
      messageType: api.spec.MessageType.WELCOME,
      message: 'Welcome to the Singularity Engine'
    });

    // eslint-disable-next-line security/detect-object-injection
    this.connections[id] = connection;
  }

  async onDisconnection(client) {
    const { /*headers, address, query, socket,*/ id /*request, write*/ } = client;
    api.log(`Client ${id} has disconnected (server)`);

    const connection = this.connections[client.id];
    if (connection) {
      for (let i = this.middleware.length - 1; i >= 0; i--) {
        // eslint-disable-next-line security/detect-object-injection
        const middleware = this.middleware[i];
        await middleware.post({ connection });
      }

      connection.moveToServerWorker(null);
    }

    // eslint-disable-next-line no-prototype-builtins
    if (this.connections.hasOwnProperty(id)) {
      // eslint-disable-next-line security/detect-object-injection
      delete this.connections[id];
    }
  }

  async start() {
    await super.start();
  }

  async stop() {
    await super.stop();
  }
}

module.exports = WebsocketServer;
