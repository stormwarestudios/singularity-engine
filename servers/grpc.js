'use strict';

const { api, Server } = require('..');

class gRPCServer extends Server {
  constructor() {
    super();
    api.servers = api.servers || {};
    api.servers.grpc = this;
    this.type = 'grpc';
  }
}

module.exports = gRPCServer;
