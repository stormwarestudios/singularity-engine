'use strict';

const http = require('http');
const path = require('path');
const url = require('url');
const fs = require('fs');
const { parse } = require('querystring');
const BrowserFingerprint = require('browser_fingerprint');

const { api, Server, Connection } = require('..');

class WebServer extends Server {
  constructor() {
    super();
    api.servers = api.servers || {};
    api.servers.web = this;
    this.type = 'web';

    this.server = http.createServer(this.requestHandler.bind(this));
    this.fingerprinter = new BrowserFingerprint({
      cookieKey: 'fingerprint',
      toSetCookie: true,
      onlyStaticElements: true,
      settings: {
        path: '/',
        expires: 3600000,
        httpOnly: null
      }
    });

    this.handlers = {};
  }

  async initialize() {
    const webProcesses = Object.values(api.processes).filter(p => p.connectionTypes.indexOf('web') !== -1);
    for (const process of webProcesses) {
      const { route: {
        method, path
      } } = process;

      // eslint-disable-next-line security/detect-object-injection
      this.handlers[method] = this.handlers[method] || {};
      // eslint-disable-next-line security/detect-object-injection
      this.handlers[method][path] = process;

      api.log(`Mapped ${method.toUpperCase()} ${path} -> ${process.name}`, 'debug');
    }
  }

  async start() {
    await super.start();
    this.server.listen(api.config.servers.web.port);
    api.log(`Listening on port ${api.config.servers.web.port}`);
  }

  streamStaticFile(req, res, filename, route) {
    const { mediaTypes } = api.config.servers.web;
    const webroot = path.join(process.cwd(), route);
    const requestedFile = path.join(webroot, filename);
    const { ext } = path.parse(requestedFile);

    // eslint-disable-next-line node/no-deprecated-api,security/detect-non-literal-fs-filename
    fs.exists(requestedFile, function(exist) {
      if (!exist) {
        // if the file is not found, return 404
        res.statusCode = 404;
        res.end('File not found');
        return;
      }

      // if is a directory search for index file matching the extention
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      fs.stat(requestedFile, (e, stats) => {
        if (e) {
          api.log(e);
          res.statusCode = 500;
          res.end('Invalid request');
          return;
        }

        if (stats.isDirectory()) {
          api.log(e);
          res.statusCode = 500;
          res.end('Invalid request');
          return;
        }

        // eslint-disable-next-line security/detect-object-injection
        res.setHeader('Content-Type', mediaTypes[ext] || 'text/plain');

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const rstream = fs.createReadStream(requestedFile);

        api.log(`GET ${filename}`, 'debug');

        rstream.pipe(res).on('error', (err) => {
          res.statusCode = 500;
          res.end(`Error getting the file: ${err}.`);
        });
      });
    });
  }

  // eslint-disable-next-line no-unused-vars
  serveStaticFile(req, res, filename, route) {
    throw new Error('not implemented');
  }

  async handleApiCall(connection, method, apiCall) {
    const start = Date.now();
    // eslint-disable-next-line no-prototype-builtins
    if (!this.handlers.hasOwnProperty(method)) {
      connection.rawConnection.res.statusCode = 404;
      throw new Error('bad method');
    }

    // eslint-disable-next-line no-prototype-builtins,security/detect-object-injection
    if (!this.handlers[method].hasOwnProperty(apiCall)) {
      connection.rawConnection.res.statusCode = 404;
      throw new Error('bad path');
    }

    // Get the related process
    // eslint-disable-next-line security/detect-object-injection
    const process = this.handlers[method][apiCall];

    const data = {
      connection,
      response: {},
      error: null
    };

    // Apply middleware pre-hooks
    try {
      // TODO: Apply middleware pre-hooks
    } catch (e) {
      api.log(e, 'error');
    }

    // Validate inputs
    try {
      // TODO: Validate inputs
    } catch (e) {
      api.log(e, 'error');
    }

    // Call process pre()
    try {
      await process.pre(data);
    } catch (e) {
      api.log(e, 'error');
    }

    // Call process exec()
    try {
      await process.exec(data);
    } catch (e) {
      api.log(e, 'error');
    }

    // Call process post()
    try {
      await process.post(data);
    } catch (e) {
      api.log(e, 'error');
    }

    // Apply middleware post-hooks
    try {
      // TODO: Apply middleware post-hooks
    } catch (e) {
      api.log(e, 'error');
    }

    const end = Date.now();
    api.log(`${method.toUpperCase()} ${apiCall} -> ${process.name} ${end - start}ms`, 'debug');

    return data;
  }

  handleFileRequest(req, res, route, routeConfig) {
    // eslint-disable-next-line node/no-deprecated-api
    const parsedUrl = url.parse(req.url);
    const { /*protocol, slashes, auth, host, port, hostname, hash, search, query, */ pathname, href } = parsedUrl;
    const { fileRequests } = routeConfig;
    let filename = pathname.split(route)[1];
    if (filename.length <= 0) {
      filename = 'index.html';
    }
    if (fileRequests === 'stream') {
      return this.streamStaticFile(req, res, filename, route, routeConfig);
    } else if (fileRequests === 'serve') {
      return this.serveStaticFile(req, res, filename, route, routeConfig);
    } else {
      return res.end();
    }
  }

  async handleApiRequest(req, res, route, routeConfig) {
    // Take browser fingerprint
    let { fingerprint } = this.fingerprinter.fingerprint(req);

    // eslint-disable-next-line node/no-deprecated-api
    const parsedUrl = url.parse(req.url);
    const { /*protocol, slashes, auth, host, port, hostname, hash, search, query, pathname,*/ href } = parsedUrl;

    // Create Connection object
    // eslint-disable-next-line node/no-deprecated-api
    const { query } = url.parse(req.url, true);
    const id = await api.engine.generateId();
    // Coerce the request into a Connection object
    const connection = new Connection(id, {
      connectedAt: new Date(),
      remoteIP: req.connection.remoteAddress,
      remotePort: req.connection.remotePort,
      rawConnection: { req, res },
      fingerprint,
      params: Object.assign({}, query, req.params, req.Body),
      type: this.type
    });
    // const apiCall = href.split('/')[2];
    const re = new RegExp(`${route}(.*)`);
    const match = href.match(re);
    if (!match) return res.end(404);
    const apiCall = match[1];
    const method = req.method.toLowerCase();

    try {
      await api.engine.runInContext(async () => {
        const result = (await this.handleApiCall(connection, method, apiCall)) || {};
        if (typeof result.response === 'object' || Object.keys(result.response).length > 0) {
          res.statusCode = res.statusCode || 200;
          res.write(JSON.stringify(result.response));
        }
        return res.end();
      });
    } catch (e) {
      res.statusCode = res.statusCode || 500;
      return res.end(e.message);
    }
  }

  parseBody(req) {
    if (req.method.toLowerCase() !== 'post') return {};

    let data = '';
    return new Promise((resolve, reject) => {
      req.on('data', (chunk) => {
        data += chunk.toString();
      });
      req.on('end', () => {
        resolve(JSON.parse(data));
      });
      req.on('error', (e) => {
        reject(e);
      });
    });
  }

  async requestHandler(req, res) {
    // eslint-disable-next-line node/no-deprecated-api
    const parsedUrl = url.parse(req.url);
    // eslint-disable-next-line no-unused-vars
    const { protocol, slashes, auth, host, port, hostname, hash, search, query, pathname, href } = parsedUrl;

    const { routes } = api.config.servers.web;

    req.Body = await this.parseBody(req);

    for (const [route, routeConfig] of Object.entries(routes)) {
      if (href.indexOf(route) === 0) {
        switch (routeConfig.type) {
          case 'file':
            return this.handleFileRequest(req, res, route, routeConfig);
          case 'api':
            return this.handleApiRequest(req, res, route, routeConfig);
        }
        break;
      }
    }

    res.statusCode = 404;
    return res.end('Not found');
  }
}

module.exports = WebServer;
