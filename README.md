# 💥 🕳️🕳️Singularity Engine 💥

A highly optimized, multi-connectivity game-server.

## Features

* streaming static files
* server-workers managed in worker_threads
* binary protobuf messages over websockets

## Components


| Component | Library | Reference  |
|---|---|---|
| Logger | Winston  | https://github.com/winstonjs/winston |
| HTTP server | Native  | https://nodejs.org/api/http.html |
| gRPC server  | Google gRPC | https://grpc.io/docs/tutorials/basic/node/ |
| Websocket server | Primus + ws | https://github.com/primus/primus |
| ORM | Sequelize | https://sequelize.org/v5/ |
|   |   |   |


## Architecture

```
+-------------------------------------------------------------+
| Singularity Engine                                          |
|                                                             |
|                     +-------------------+                   |
|                     |                   |                   |
|                     |  Client Request   |                   |
|                     |                   |                   |
|                     +--+-------------^--+                   |
|                        |             |                      |
|                        |             |                      |
|                        |             |                      |
|                        |             |                      |
|                      +-v-------------+-+                    |
|                      |                 |                    |
|                      |   "The Cloud"   |                    |
|                      |                 |                    |
|                      +-+-------------^-+                    |
|                        |             |                      |
|                        |             |                      |
|  +---------------------|-------------|-------------------+  |
|  |                     |             |                   |  |
|  | Servers (Web, Websocket, gRPC, Socket)                |  |
|  |                     |             |                   |  |
|  |  +------------------|-------------|----------------+  |  |
|  |  |                  |             |                |  |  |
|  |  |                  Client Request|                |  |  |
|  |  |                  |             |                |  |  |
|  |  | +----------------v---+  +------+-------------+  |  |  |
|  |  | |                    |  |                    |  |  |  |
|  |  | | Connection Builder |  | Response Decorator |  |  |  |
|  |  | |                    |  |                    |  |  |  |
|  |  | +--------------+-----+  +------^-------------+  |  |  |
|  |  |                |               |                |  |  |
|  |  +----------------|---------------|----------------+  |  |
|  |                   |               |                   |  |
|  +-------------------|---------------|-------------------+  |
|                      |               |                      |
|                      |               |                      |
|                    +-v---------------+-+                    |
|                    |                   |                    |
|                    | Connection Object |                    |
|                    |                   |                    |
|                    +-+---------------^-+                    |
|                      |               |                      |
|                      |               |                      |
|  +-------------------|---------------|-------------------+  |
|  |                   |               |                   |  |
|  | Middleware        |               |                   |  |
|  |                   |               |                   |  |
|  |    +--------------v------+  +-----+----------------+  |  |
|  |    |                     |  |                      |  |  |
|  |    | Middleware Pre Hook |  | Middleware Post Hook |  |  |
|  |    |                     |  |                      |  |  |
|  |    +-------------+-------+  +-----^----------------+  |  |
|  |                  |                |                   |  |
|  +------------------|----------------|-------------------+  |
|                     |                |                      |
|                     |                |                      |
|  +------------------v----------------+-------------------+  |
|  |                                                       |  |
|  | Process                                               |  |
|  |                                                       |  |
|  |  +-----------------+-^-+      +-+-^-----------------+ |  |
|  |  |                 | | |      | | |                 | |  |
|  |  | Server Worker   v + |      | v +   Server Worker | |  |
|  |  |                     |      |                     | |  |
|  |  |                     |      |                     | |  |
|  |  |                     |      |                     | |  |
|  +--+                     +------+                     +-+  |
|     |                     |      |                     |    |
|     |                     |      |                     |    |
|     |                     |      |                     |    |
|     |                     |      |                     |    |
|     +-+-----^-----+----^--+      +-+-----^-----+----^--+    |
|       |     |     |    |           |     |     |    |       |
|       |     |     |    |           |     |     |    |       |
|       |     |     |    |           |     |     |    |       |
|     +-v-----++  +-v----+-+       +-v-----++  +-v----+-+     |
|     |        |  |        |       |        |  |        |     |
|     | Client |  | Client |       | Client |  | Client |     |
|     | Worker |  | Worker |       | Worker |  | Worker |     |
|     |        |  |        |       |        |  |        |     |
|     +--------+  +--------+       +--------+  +--------+     |
|                                                             |
+-------------------------------------------------------------+
```
