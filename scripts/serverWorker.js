/* eslint-disable */

'use strict';

const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const Sequelize = require('sequelize');
const { Database } = require('../../lib/core/Database');
const Redis = require('ioredis');

// global.Ammo = require('ammo');
// global.OIMO = require('oimo');
global.CANNON = require('cannon');

const BABYLON = require('babylonjs');
const { World } = require('ecsy');
const { Object3D, Moving } = require('../../lib/component');
const { Command } = require('../../lib/command');
const { MovingSystem } = require('../../lib/system');
const { Assemblage } = require('../../lib/assemblage');

const {
  // NullEngine,
  // Scene,
  // PointLight,
  // Vector3,
  // ArcRotateCamera,
  // OimoJSPlugin,
  // AmmoJSPlugin,
  CannonJSPlugin,
  // SceneLoader: {
  //   ImportMeshAsync
  // }
} = BABYLON;

const DEFAULT_FPS = 10;

const { ClientWorker, FSM_STATES, DEFAULT_STATE } = require('./ClientWorker');

class ServerWorker {
  #id = 0;
  #messagePort = null;
  #data = null;
  #timer = null;
  #fps = DEFAULT_FPS;
  #lastUpdate = null;

  #database = null;

  #engine = null;
  #scene = null;
  #camera = null;
  #rootMesh = null;
  #lastTime = 0;
  // #timeAccumulator = 0;
  // #timeBetweenUpdates = 1000 / this.#fps;

  #world = null;

  #role = null;
  #clientWorkers = null;
  #level = null;
  #state = null;

  #commandQueue = [];
  #eventQueue = [];

  #commandTimer = null;
  #eventTimer = null;

  #redis = null;
  #pub = null;
  #sub = null;
  #cache = null;

  constructor(options) {
    const { messagePort, data } = options;
    const { config, level, state } = data;
    const { redis } = config;

    this.#redis = redis;

    this.#level = level;
    this.#state = state;

    this.#id = data.id;
    this.#messagePort = messagePort;

    this.#messagePort.on('message', this.onMessagePortMessage.bind(this));
    this.#messagePort.on('close', this.onMessagePortClose.bind(this));

    this.#data = data;
    this.#fps = config.fps || DEFAULT_FPS;
    // this.#timeBetweenUpdates = 1000 / this.#fps;
    // this.#timeAccumulator = 0;
    this.#lastUpdate = Date.now();
    this.#database = null;
    this.#clientWorkers = {};

    this.#engine = new BABYLON.NullEngine({
      deterministicLockstep: true,
      lockstepMaxSteps: 4
    }); // TODO: Redirect logging to Winston
    this.#scene = new BABYLON.Scene(this.#engine);
    this.#camera = new BABYLON.ArcRotateCamera(`Camera ${this.#id}`, 0, 0.8, 1, BABYLON.Vector3.Zero(), this.#scene);

    // Enable physics engine
    let physicsEngine = null;
    // try { physicsEngine = new OimoJSPlugin() } catch (e) { console.log(e) }
    // try { physicsEngine = new AmmoJSPlugin() } catch (e) { console.log(e) }
    try { physicsEngine = new CannonJSPlugin() } catch (e) { console.log(e) }
    try { this.#scene.enablePhysics(undefined, physicsEngine) } catch (e) { console.log(e); }

    this.#world = new World(); // a whole new world
    this.#rootMesh = BABYLON.MeshBuilder.CreateBox('box', { size: 1 }, this.#scene);

    // Register components
    // this.log(`ECS components:`);
    for (const component of [Object3D, Moving]) {
      this.#world.registerComponent(component);
      // this.log(`  - ${component.name}`);
    }

    // Register systems
    // this.log(`ECS systems:`);
    for (const system of [MovingSystem]) {
      this.#world.registerSystem(system);
      // this.log(`  - ${system.name}`);
    }

    this.#commandTimer = setInterval(async () => {
      await this.processCommand();
    }, 1);
    this.#eventTimer = setInterval(async () => {
      await this.processEvent();
    }, 1);
  }

  async initializeRedis() {
    const p = [];
    const { port, host, password, db } = this.#redis;

    /**
     * Create a Redis client with predetermined settings.
     * @param {string} name The name of the connection.
     * @returns {Redis}
     */
    const createRedisClient = (name) => {
      const options = {
        port,
        host,
        password,
        db,
        connectionName: `${this.#id}.${name}`,
        dropBufferSupport: true,
        enableReadyCheck: true,
        enableOfflineQueue: true,
        autoResubscribe: true,
        lazyConnect: true
      };

      return new Redis(options);
    };

    this.#pub = createRedisClient('pub');
    this.#sub = createRedisClient('sub');
    this.#cache = createRedisClient('cache');

    p.push(this.#pub.connect());
    p.push(this.#sub.connect());
    p.push(this.#cache.connect());

    return await Promise.all(p);
  }

  getRedis() {
    return {
      pub: this.#pub,
      sub: this.#sub,
      cache: this.#cache
    };
  }

  async processCommand() {
    const command = this.#commandQueue.shift();
    if (!command) return;

    try {
      await this.#clientWorkers[command.connection].command(command.name, command.args, command.targetEntity);
    } catch (e) {
      this.log(e.message, 'error', null);
      // TODO: requeue command?
    }

    // Check command.target entity restrictions

    // 1. ComponentsRequired
    // 2. Distance (less than 'max', greater than 'min)
    // 3.
  }

  async processEvent() {
    const event = this.#eventQueue.shift();
    if (!event) return;
  }

  onMessagePortMessage(message) {
    // console.log(`ClientWorker ${this.#id} received message ${JSON.stringify(message)}`);
    const { connection, context, command, args, entity } = message;
    switch (context) {
      case 'user':
        if (this.#clientWorkers.hasOwnProperty(connection)) {
          this.log(`Sending command to ClientWorker: connection='${connection}' command='${command}' args='${args}' entity='${entity}'`);
          this.#commandQueue.push(new Command({
            name: command,
            targetEntity: entity,
            sourceEntity: null,
            args,
            connection,
            context,
            createdAt: performance.now()
          }));
        } else {
          console.log(`Unknown connection='${connection}', command='${command}', entity='${entity}', args='${JSON.stringify(args)}'`);
        }
        break;
      case 'system':
        this[command].apply(this, args);
        break;
      default:
        this.log(`Invalid context '${context}'`, 'error', message);
        break;
    }
  }

  addConnection(connectionId, metadata) {
    if (this.#clientWorkers.hasOwnProperty(connectionId)) {
      this.log(`Adding connection ${connectionId} failed; already exists`, 'error', null);
    } else {
      this.#clientWorkers[connectionId] = new ClientWorker(this, connectionId, this.#level, this.#state, metadata);
      this.log(`Adding connection ${connectionId} succeeded`, 'info', null);
    }
  }

  removeConnection(connectionId) {
    if (this.#clientWorkers.hasOwnProperty(connectionId)) {
      delete this.#clientWorkers[connectionId];
      this.log(`Removed connection ${connectionId}`, 'info', null);
    } else {
      this.log(`Removing connection ${connectionId} failed; connection does not exist`, 'error', null);
    }
  }

  onMessagePortClose() {
    console.log(`ClientWorker ${this.#id} messagePort has closed.`);
  }

  /**
   * Convenience function to use master thread's logging facility
   * @param text
   * @param level
   * @param obj
   */
  log(text, level = 'info', obj = null) {
    this.#messagePort.postMessage({
      call: [
        {
          fn: 'log',
          args: [`[${this.#id}] ${text}`, level, obj]
        }
      ]
    });
  }

  /**
   * Initialize and verify connection to the database
   * @returns {Promise<void>}
   */
  async initializeDatabase() {
    const {
      config: { database }
    } = this.#data;
    const cfg = database; // to keep the code looking like elsewhere until we can refactor
    this.#database = await Database.getConnection(cfg, (text) => {
      this.log(`${text}`, 'info', {});
    });
  }

  /**
   * Database connection getter.
   * @returns {Sequelize|null} A valid Sequelize object, if one is available; null otherwise.
   */
  getDatabase() {
    return this.#database;
  }

  getSequelize() {
    return Sequelize;
  }

  /**
   * Generate dummy box entities in the scene
   * @param entityCount
   */
  generateTestData(entityCount) {
    for (let i = 0; i < entityCount; i++) {
      const entity = Assemblage.createEntity(this.#world, this.log.bind(this), 'WorldObject', {
        Object3D: { objectName: 'box', object: this.#rootMesh.createInstance('box') },
        Moving: { velocity: { x: 0.1, y: 0.1, z: 0.1} }
      });
    }
  }

  /**
   * Call a single function on the main thread.
   * @param {String} fn The dotted function name to be called on the main thread.
   * @param {Array} args The list of arguments to be passed to the function called on the main thread.
   */
  postMessage(fn, args) {
    this.#messagePort.postMessage({ call: [{ fn, args }]});
  }

  /**
   * Call a list of functions on the main thread,
   * @param {Object[]} callList The list of functions and their arguments to be called on the main thread.
   */
  postMessages(callList) {
    this.#messagePort.postMessage({ call: callList });
  }

  async start() {
    this.log(`Starting ServerWorker ID=${this.#id} level=${this.#level.name}`);
    await this.initializeDatabase();

    await this.initializeRedis();

    this.generateTestData(0);

    this.#world.execute(0, 0);

    let loopIterations = 0;
    this.#lastTime = performance.now() / 1000;
    this.#engine.runRenderLoop(() => {
      let time = performance.now() / 1000;
      let delta = time - this.#lastTime;
      this.#lastTime = time;

      // TODO: Limit rendering and updates to this.#fps
      this.#scene.render({
        updateCameras: false,
        ignoreAnimations: true
      });

      this.#world.execute(delta, time);

      loopIterations++;
    });
  }

  async stop() {
    if (this.#timer) {
      clearInterval(this.#timer);
      this.#timer = null;
    }
  }

  getWorld() {
    return this.#world;
  }

  getScene() {
    return this.#scene;
  }

  getEngine() {
    return this.#engine;
  }

  getRole() {
    return this.#role;
  }

  async tick(f) {
    // TODO: Apply systems to entities
    // await this.#ecs.tick();
    // this.log(`Update (f=${f.toFixed(3)}s)`, 'info', {});
  }
}

(async () => {
  if (isMainThread) {
    // Do nothing
  } else {
    const sw = new ServerWorker({
      messagePort: parentPort,
      data: workerData
    });
    await sw.start();
  }
})();

module.exports = { ServerWorker };
