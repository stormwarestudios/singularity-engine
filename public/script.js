'use strict';

let client = null;
let spec = null;

const EMAIL_ADDRESS= 'testaccount@galactikore.com';
const PASSWORD = 'Pass1word?';

function sendMessage(client, payload, type) {
  const errMsg = type.verify(payload);
  if (errMsg) throw Error(errMsg);
  const message = type.fromObject(payload);
  // const tmp = type.encode(message).finish();
  const uint8Array = type.encode(message).finish();
  client.write(uint8Array.slice().buffer);
}

async function login(emailAddress, password) {
  const body = JSON.stringify({
    emailAddress, password
  });

  // eslint-disable-next-line no-undef
  const rawResponse = await fetch('/api/auth/login', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body
  });
  return await rawResponse.json();
}

async function main() {
  await login(EMAIL_ADDRESS, PASSWORD);

  await websocket();
}

// eslint-disable-next-line no-unused-vars
async function websocket() {
  console.log(`It works!`); //eslint-disable-line quotes

  // eslint-disable-next-line no-undef
  const { nested: {
    singularity
  }} = await protobuf.load('/proto/client.proto');
  spec = singularity;

  // eslint-disable-next-line no-undef,quotes
  client = new Primus(`ws://${window.location.host}`, {
    reconnect: {
      max: Infinity, // Number: The max delay before we try to reconnect.
      min: 500, // Number: The minimum delay before we try reconnect.
      retries: 10 // Number: How many times we should try to reconnect.
    },
    timeout: 10000,
    pingTimeout: 45000,
    strategy: ['online', 'timeout', 'disconnect'],
    manual: true,
    websockets: true,
    network: true,
    transport: {},
    queueSize: Infinity
  });

  client.on('data', (data) => {
    const buffer = new Uint8Array(data);
    data = spec.Base.decode(buffer);
    switch (data.messageType) {
      case spec.MessageType.WELCOME:
        data = spec.Welcome.decode(buffer);
    }

    // Let the server know we are ready
    sendMessage(
      client,
      {
        messageType: spec.MessageType.READY,
        ready: true
      },
      spec.Ready
    );
  });

  client.on('open', () => {
    console.debug('open');
  });
  client.on('error', () => {
    console.debug('error');
  });
  client.on('reconnect', () => {
    console.debug('reconnect');
  });
  client.on('reconnect scheduled', () => {
    console.debug('reconnect scheduled');
  });
  client.on('reconnected', () => {
    console.debug('reconnected');
  });
  client.on('reconnect timeout', () => {
    console.debug('reconnect timeout');
  });
  client.on('reconnect failed', () => {
    console.debug('reconnect failed');
  });
  client.on('end', () => {
    console.debug('end');
  });

  client.open();
}
