'use strict';

const { api, GameState } = require('../..');

module.exports = class Boot extends GameState {
  async _onEnter() {
    await super._onEnter();
    this.transition('MainMenu');
  }

  async _onExit() {
    await super._onExit();
  }

  onKeyDown(e) {
    //console.log(e);
  }

  onKeyUp(e) {
    //console.log(e);
  }

  onKeyPress(e) {
    console.log(e);
  }

  cleanup() {
    console.log('Boot::cleanup()');
  }
};
