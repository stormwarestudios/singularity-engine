/* eslint-disable security/detect-non-literal-require */

'use strict';

let api;
const path = require('path');
const merge = require('deepmerge');
const assemblages = require(path.join(process.cwd(), 'lib', 'ecs', 'assemblage', 'assemblage.json'));
const Components = require(path.join(process.cwd(), 'lib', 'ecs', 'component'));

class Assemblage {
  world = null;

  constructor(world) {
    api = require('../../../index').api;
    this.world = world;
  }

  include(name, accumulator) {
    // eslint-disable-next-line security/detect-object-injection
    const assemblage = assemblages[name];
    if (assemblage) {
      // eslint-disable-next-line no-prototype-builtins
      if (assemblage.hasOwnProperty('$include')) {
        // get children
        const includes = assemblage['$include'];

        // iterate through children
        for (const include of includes) {
          // call this.include(childName, accumulator)
          this.include(include, accumulator);
        }
      }

      // include parent
      accumulator.push(assemblage);
    } else {
      api.log(`No assemblage with name '${name}'; skipping`);
    }
  }

  createEntityFromAssemblage(assemblageName, componentData = {}) {
    // Prepare an accumulator
    const assemblages = [];

    // Build accumulated assemblages
    this.include(assemblageName, assemblages);

    // Add the componentData to the end, treating them as overrides
    assemblages.push(componentData);

    // Merge left-to-right, so that later configured entities' components override included cones
    const merged = merge.all(assemblages);

    merged.Assemblage = { name: assemblageName };

    // Create our entity
    const entity = this.world.createEntity();
    api.log(`Entity id=${entity.id}`);

    // For each component
    for (const key of Object.keys(merged).sort()) {
      // Ignore '$include' attributes
      if (key === '$include') continue;

      // Get the Component override values
      // eslint-disable-next-line security/detect-object-injection
      const value = merged[key];

      // eslint-disable-next-line no-prototype-builtins
      if (!Components.hasOwnProperty(key)) {
        api.log(`WARNING: Component '${key}' does not exist; skipping`);
        continue;
      }

      // Get the Component
      // eslint-disable-next-line security/detect-object-injection
      const component = Components[key];

      // Add the Component, along with the override values, to the Entity
      entity.addComponent(component, value);
    }

    return entity;
  }
}

module.exports = {
  Assemblage
};
