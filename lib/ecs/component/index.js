'use strict';

class Component {
  import(data) {
    Object.assign(this, data);
  }

  export() {
    return Object.parse(Object.stringify(this));
  }
}

class Account extends Component {
  constructor() {
    super();
    this.id = null;
  }

  reset() {
    this.id = null;
  }
}

class Collisionable extends Component {}

class Collider extends Component {}

class Recovering extends Component {}

class Moving extends Component {
  constructor() {
    super();
    this.velocity = {
      x: 0, y: 0, z: 0
    }
  }

  reset() {
    this.velocity = {
      x: 0, y: 0, z: 0
    }
  }
}

class PulsatingScale extends Component {
  constructor() {
    super();
    this.offset = 0;
  }
}

class Object3D extends Component {
  constructor() {
    super();
    this.meshName = 'dummy3.babylon';
    this.object = null;
  }

  reset() {
    this.object = null;
    this.meshName = 'dummy3.babylon';
  }

  import() {}

  export() {
    return {
      object: null,
      meshName: this.meshName
    };
  }
}

class Timeout extends Component {
  constructor() {
    super();
    this.timer = 0;
    this.addComponents = [];
    this.removeComponents = [];
  }
}

class PulsatingColor extends Component {
  constructor() {
    super();
    this.offset = 0;
  }
}

class Colliding extends Component {
  constructor() {
    super();
    this.value = false;
  }
}

class Rotating extends Component {
  constructor() {
    super();
    this.enabled = true;
    this.rotatingSpeed = 0;
    this.decreasingSpeed = 0.001;
  }
}

class Position extends Component {
  constructor() {
    super();
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  reset() {
    this.x = this.y = this.z = 0;
  }
}

class Velocity extends Component {
  constructor() {
    super();
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  reset() {
    this.x = this.y = this.z = 0;
  }
}

class Persistence extends Component {
  constructor() {
    super();
    this.enabled = true;
  }

  reset() {
    this.enabled = true;
  }
}

class EntityAcl extends Component {
  constructor() {
    super();

    /**
     * List of ServerWorker IDs which have write-access to the owning Entity.
     * @type {Array.<String>} An array of UUIDs representing ServerWorker IDs which have write authority over this Entity.
     * When a ServerWorker attempts to perform updates on this Entity, it will first consult the EntityAcl.write list to
     * ensure that its own ID is found here.
     */
    this.write = [];

    /**
     * List of ServerWorker IDs which have read-access to the owning Entity.
     * @type {Array.<String>} An array of UUIDs representing ServerWorker IDs which have read authority over this Entity.
     * When an update occurs on this Entity, its owning ServerWorker will broadcast details about the Entity to PubSub
     * channels which match the ServerWorker IDs stored here.
     */
    this.read = [];
  }

  reset() {
    this.read.length = 0;
    this.write.length = 0;
  }
}

class Metadata extends Component {
  constructor() {
    super();
    this.label = '';
  }

  reset() {
    this.label = '';
  }
}

class Interest extends Component {
  constructor() {
    super();
    this.interest = [];
  }

  reset() {
    this.interest.length = 0;
  }
}

class Location extends Component {
  constructor() {
    super();

    this.dimension = '';
    this.galaxy = '';
    this.sector = [0, 0];
  }

  reset() {
    this.dimension = '';
    this.galaxy = '';
    this.sector = [0, 0];
  }
}

class Character extends Component {
  constructor() {
    super();

    this.givenName = '';
    this.surname = '';
    this.middleNames = [];
    this.origin = 'Sol III';
    this.species = 'Terran';
    this.gender = 'Other';
    this.careers = {
      default: {
        rank: 'Civilian',
        actingRank: 'none',
        commission: 'none',
        profession: 'none',
        experience: 0,
        aptitude: []
      }
    };
  }
}

class Assemblage extends Component {
  constructor() {
    super();
    this.name = '';
  }

  reset() {
    this.name = '';
  }
}

class Crew extends Component {
  constructor() {
    super();
    this.captain = null;
    this.members = [];
  }
}

class SubSystem extends Component {
  constructor() {
    super();

    /**
     * Type of subsystem (active or passive).
     * Passive subsystems regenerate to their maximum capacity, but deplete their energy source each cycle to perform
     * their normal function.
     * Active subsystems regenerate to their maximum capacity, but must be discharged manually.
     * @type {number}
     */
    this.type = 'passive'; // 'active' or 'passive'

    /**
     * The SubSystem's accumulated (or "currently stored") energy level.
     * @type {number}
     */
    this.energy = 0;

    /**
     * The rate at which this SubSystem recharges.
     * @type {number}
     */
    this.rechargeRate = 0.025;

    /**
     * The rate at which, if passive, this SubSystem discharges.
     * @type {number}
     */
    this.dischargeRate = 0.015;

    /**
     * The maximum amount of stored power this SubSystem can attain.
     * @type {number}
     */
    this.maximumEnergy  = 1.0;

    /**
     * The amount of power per cycle this SubSystem is allocated.
     * @type {number}
     */
    this.power              = 0;

    /**
     * This SubSystem's condition, which directly impacts its performance.
     * @type {number}
     */
    this.condition          = 1.0;

    /**
     * The delta between this SubSystem's peak operating temperature
     * and its critical threshold for thermal damage.
     * @type {number}
     */
    this.temperature        = 0;

    /**
     * The maximum temperature this SubSystem can reach before it starts
     * experiencing thermal damage.
     * @type {number}
     */
    this.maximumTemperature = 1.0;

    /**
     * The amount of coolant allocated to this SubSystem to reduce its
     * temperature and prevent damage.
     * @type {number}
     */
    this.coolant             = 0.0;

    /**
     * The amount of repair resources allocated to this SubSystem to increase its condition and effectiveness.
     * @type {number}
     */
    this.repairs              = 0.0;

    /**
     * Determines whether this SubSystem is undergoing repairs, allowing
     * its condition to increase per-cycle.
     * @type {boolean}
     */
    this.isBeingRepaired    = false;

    this.state              = 'functional';
  }

  reset() {
    this.type               = 'passive';
    this.energy             = 0;
    this.rechargeRate       = 0.025;
    this.dischargeRate      = 0.015;
    this.maximumEnergy      = 1.0;
    this.power              = 0;
    this.condition          = 1.0;
    this.temperature        = 0;
    this.maximumTemperature = 1.0;
    this.coolant            = 0.0;
    this.repairs            = 0.0;
    this.isBeingRepaired    = false;
    this.state              = 'functional';
  }
}

/*class WorkStation extends Component {
  constructor() {
    super();
    this.efficiency = 1.0;
    this.condition = 1.0;
    this.requiredProfession = null;
    this.requiredRank = null;
  }
}

class ScienceWorkStation extends WorkStation {}
class CommunicationsWorkStation extends WorkStation {}
class TacticalWorkStation extends WorkStation {}
class NavigationWorkStation extends WorkStation {}
class EngineeringWorkStation extends WorkStation {}
class CommandWorkStation extends WorkStation {}*/

module.exports = {
  Account,
  Collisionable,
  Collider,
  Recovering,
  Moving,
  PulsatingScale,
  Object3D,
  Timeout,
  PulsatingColor,
  Colliding,
  Rotating,
  Position,
  Velocity,
  Persistence,
  EntityAcl,
  Metadata,
  Interest,
  Assemblage,
  Location,
  Crew,
  SubSystem,
  Character
  /*ScienceWorkStation,
  CommunicationsWorkStation,
  TacticalWorkStation,
  NavigationWorkStation,
  EngineeringWorkStation,
  CommandWorkStation*/
};
