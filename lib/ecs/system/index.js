'use strict';

const { System } = require('ecsy');

const { Object3D, Collisionable, Collider, Recovering, Moving, PulsatingScale, Timeout, PulsatingColor, Colliding, Rotating, Position, Velocity, Persistence, SubSystem } = require('../component');

class MovingSystem extends System {
  execute(delta, time) {
    let entities = this.queries.entities.results;
    for (let i = 0; i < entities.length; i++) {
      let entity = entities[i];
      let { object } = entity.getComponent(Object3D);
      let { velocity } = entity.getComponent(Moving);

      if (!object) continue;

      object.position.x += velocity.x * delta;
      object.position.y += velocity.y * delta;
      object.position.z += velocity.z * delta;

      console.log(`Entity ${entity.id} x=${object.position.x.toFixed(3)} y=${object.position.y.toFixed(3)} z=${object.position.z.toFixed(3)}`);
    }
  }
}

MovingSystem.queries = {
  entities: { components: [Moving, Object3D] }
};

class PersistenceSystem extends System {
  execute(delta, time) {
    let entities = this.queries.entities.results;
    for (let i = 0; i < entities.length; i++) {
      let entity = entities[i];
      // TODO: Export and persist Entity
    }
  }
}

PersistenceSystem.queries = {
  entities: { components: [Persistence] }
};

const TEMPERATURE_DELTA = 0.1;
const THERMAL_DAMAGE_DELTA = 100;

class StarshipUpdateSystem extends System {
  execute(delta, time) {
    let entities = this.queries.entities.results;
    for (let i = 0; i < entities.length; i++) {
      const entity = entities[i];
      const subsystem = entity.getComponent(SubSystem);

      debugger;

      // Passive discharge
      if (subsystem.type === 'passive') {
        subsystem.energy -= subsystem.dischargeRate * delta;
        if (subsystem.energy < 0) {
          subsystem.energy = 0;
        }
      }

      // Thermalize
      let temperatureDelta = -TEMPERATURE_DELTA;
      if (subsystem.power > 1.0) {
        temperatureDelta = TEMPERATURE_DELTA;
      }

      subsystem.temperature += temperatureDelta * time;

      subsystem.temperature -= subsystem.coolant * time;
      if (subsystem.temperature < 0) {
        subsystem.temperature = 0;
      }

      if (subsystem.temperature >= subsystem.maximumTemperature) {
        subsystem.condition -= time / THERMAL_DAMAGE_DELTA;
        if (subsystem.condition < 0) {
          subsystem.condition = 0;
        }
      }

      // Repair
      if (subsystem.state !== 'destroyed' && subsystem.repairs > 0) {
        subsystem.condition += delta * subsystem.repairs;

        if (subsystem.condition >= 1.0) {
          subsystem.repairs = 0.0; // TODO: should we auto-adjust repair once complete?
          subsystem.condition = 1.0;
        }

        // Update state according to condition
        let c = subsystem.condition;
        if (c >= 1.0) {
          subsystem.state = 'functional';
        } else if (c >= 0.20) {
          subsystem.state = 'damaged';
        } else if (c >= 0.01) {
          subsystem.state = 'inoperable';
        } else if (c >= 0.0) {
          subsystem.state = 'destroyed';
        } else {
          subsystem.state = 'invalid';
        }
      }

      // Recharge
      subsystem.energy += subsystem.rechargeRate * delta * subsystem.condition * subsystem.power;
      if (subsystem.energy > subsystem.maximumEnergy * subsystem.power) {
        subsystem.energy = subsystem.maximumEnergy;
      }
    }
  }
}

StarshipUpdateSystem.queries = {
  entities: { components: [SubSystem] }
};

module.exports = {
  MovingSystem,
  PersistenceSystem,
  StarshipUpdateSystem
};
