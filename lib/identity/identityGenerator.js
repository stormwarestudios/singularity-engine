'use strict';

const generate = require('nanoid/async/generate');
const generateSync = require('nanoid/generate');
const dictionary = require('nanoid-dictionary');

const IdGenerator = Object.freeze({
  ALPHABET: dictionary.lowercase + dictionary.uppercase + dictionary.numbers,
  LENGTH: 16
});

class IdentityGenerator {
  static async generateAsync(length = IdGenerator.LENGTH) {
    return generate(IdGenerator.ALPHABET, length);
  }

  static generate(length = IdGenerator.LENGTH) {
    return generateSync(IdGenerator.ALPHABET, length)
  }
}


module.exports = IdentityGenerator;
