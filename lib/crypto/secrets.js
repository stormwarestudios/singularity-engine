'use strict';

const path = require('path');
const fs = require('fs');

let api;

module.exports = class Secrets {
  constructor() {
    api = require('../..').api;
  }

  getSecret(type) {
    // eslint-disable-next-line security/detect-object-injection
    const { secretFile } = api.config.secrets[type];
    const file = path.join(process.cwd(), secretFile);
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      fs.readFile(file, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      });
    });
  }
};
