'use strict';

// eslint-disable-next-line node/no-unsupported-features/node-builtins
const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const path = require('path');
const fs = require('fs');
const dot = require('dot-object');
const merge = require('deepmerge');
const glob = require('glob');
const { createNamespace } = require('cls-hooked');
const { pascalCase } = require('change-case');
const IdentityGenerator = require('../identity/identityGenerator');

let api;

class Engine {
  constructor(options = {}) {
    api = require('../../index.js').api;
    api.engine = this;

    api.session = IdentityGenerator.generate();
    api.namespace = createNamespace(api.session);
    api.threadName = workerData && workerData.threadName ? workerData.threadName : null;
    api.fsm = {};

    const { messagePort, data } = options;
    if (messagePort) {
      this.messagePort = messagePort;
      this.messagePort.on('message', this.onMessagePortMessage.bind(this));
      this.messagePort.on('close', this.onMessagePortClose.bind(this));
    }

    if (data) {
      this.data = data;
    }

    this.messageQueue = [];
    this.map = null;

    // Short-circuit the logging function
    const { Logger } = require('../../index');
    api.log = Logger.log;

    const { Secrets } = require('../../index');
    api.secrets = new Secrets();

    // console.log = (text) => {
    //   api.log(text, 'info');
    // };
  }

  loadConfig() {
    require('dotenv').config();

    const relativeConfigPath = path.join('config', 'default.json');
    const defaultConfigPath = path.join(process.cwd(), relativeConfigPath);

    api.log(`Loading default config from ${relativeConfigPath}`);

    // eslint-disable-next-line security/detect-non-literal-require
    const defaultConfig = require(defaultConfigPath);
    const applicationConfigPath = path.join(process.cwd(), 'config', 'config.json');
    const applicationConfig = fs.existsSync(applicationConfigPath) ? require(applicationConfigPath) : {};

    // Merge application config -> default config
    api.config = merge.all([defaultConfig, applicationConfig]);

    // Obtain environment variables with prefix found in application.id
    const {
      application: { id: applicationId }
    } = api.config;

    const prefix = `${applicationId.toUpperCase()}_`;
    api.log(`Merging environment variables with prefix ${prefix}`, 'debug');
    const env = {};

    Object.keys(process.env)
      .sort()
      .filter((entry) => {
        return entry.startsWith(prefix);
      })
      .forEach((entry) => {
        const key = entry.toLowerCase().replace(/_/gi, '.');
        env[key] = process.env[entry];
        api.log(`${entry} -> ${key}`, 'debug');
      });

    // Extract the top-layer for merging
    const { [applicationId]: environmentConfig } = dot.object(env);

    // Merge env config -> application config
    if (environmentConfig) {
      api.config = merge.all([api.config, environmentConfig]);
    }

    // Perform file replacements for secrets loaded into the filesystem
    const {
      secrets: { fileReplacements }
    } = api.config;

    const replacements = {};

    for (const entry of fileReplacements) {
      const value = dot.pick(entry, api.config);
      if (!fs.existsSync(value)) {
        api.log(`Secret file-replacement '${entry}' was specified for '${value}', but no file exists`, 'warn');
        continue;
      }

      replacements[entry] = fs
        .readFileSync(value, {
          encoding: 'utf-8',
          flag: 'r'
        })
        .trim();
    }

    api.config = merge(api.config, dot.object(replacements));
  }

  loadServers() {
    for (const [type, config] of Object.entries(api.config.servers)) {
      if (config.enabled === true) {
        api.log(`Server '${type}' enabled`, 'debug');
        api.servers = api.servers || {};
        // eslint-disable-next-line security/detect-non-literal-require
        const Server = require(`../../servers/${type}`);
        // eslint-disable-next-line security/detect-object-injection
        api.servers[type] = new Server();
      } else {
        api.log(`Server '${type}' disabled`, 'debug');
      }
    }
  }

  async loadStates() {
    return new Promise((resolve, reject) => {
      const {
        application: {
          fsm: { states }
        }
      } = api.config;
      const statePath = path.join(process.cwd(), states);
      glob(`${statePath}/**/*.js`, {}, (e, matches) => {
        if (e) {
          api.log(e);
          return reject(e);
        }
        for (const match of matches) {
          // eslint-disable-next-line security/detect-non-literal-require
          const GameState = require(match);
          // eslint-disable-next-line security/detect-object-injection
          api.fsm[GameState.name] = GameState;
        }
        resolve();
      });
    });
  }

  async loadProcesses() {
    const {
      application: {
        folders: { processes }
      }
    } = api.config;

    const processesPath = path.join(process.cwd(), processes);

    return new Promise((resolve, reject) => {
      glob(`${processesPath}/**/*.js`, {}, (err, matches) => {
        if (err) {
          return reject(err);
        } else {
          api.processes = api.processes || {};
          for (const entry of matches) {
            // eslint-disable-next-line security/detect-non-literal-require
            const Process = require(entry);
            const process = new Process();
            api.processes[process.name] = process;
            api.log(`Process '${process.name}' loaded`);
          }
          resolve();
        }
      });
    });
  }

  async loadMiddleware() {
    for (const [type, server] of Object.entries(api.servers)) {
      for (const [name, process] of Object.entries(api.processes)) {
        if (process.hooks.indexOf('connection') === -1) continue;
        if (process.connectionTypes.indexOf(type) === -1) continue;
        api.log(`${type} ${name}`, 'debug');
        server.middleware.push(process);
      }
      // Sort middleware by priority
      server.middleware.sort((a, b) => (a.priority > b.priority ? 1 : -1));
    }
  }

  async generateId(length) {
    // return generate(IdGenerator.ALPHABET, length);
    return IdentityGenerator.generateAsync(length);
  }

  generateIdSync(length) {
    // return generateSync(IdGenerator.ALPHABET, length);
    return IdentityGenerator.generate(length);
  }

  async initialize() {
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      api.namespace.run(async () => {
        try {
          api.namespace.set('context', api.session);

          if (isMainThread) {
            const pjson = require('../../package.json');
            api.log(`Welcome to the Singularity Engine v${pjson.version}`);
          }

          this.loadConfig();
          await this.loadSpec();

          api.log('Engine initializing');

          if (isMainThread) {
            await this.initializeThreads();

            await this.loadServers();
          }

          await this.initializeRedis();

          await this.loadDatabase();
          if (api.config.scheduler.enabled === true) {
            await this.loadScheduler();
          }

          await this.loadProcesses();

          if (isMainThread) {
            // eslint-disable-next-line no-unused-vars
            for (const server of Object.values(api.servers)) {
              await server.initialize();
            }

            await this.loadMiddleware();
          }

          await api.scheduler.start();

          await this.initializeHooks();

          if (!isMainThread) {
            await this.loadStates();

            const { Map } = require('../../index');
            this.map = new Map({
              serverWorker: this,
              state: '',
              mapId: '',
              locationId: workerData.serverWorkerName,
              parentId: null
            });
            await this.map.initialize();
          }

          resolve();
        } catch (e) {
          api.log(e);
          reject(e);
          process.exit(1);
        }
      });
    });
  }

  async loadDatabase() {
    const { Database } = require('../../index');
    api.database = new Database();
    await api.database.initialize();
  }

  /*getDefaultServerWorker() {
    const {
      application: {
        serverWorkers: { defaultWorker }
      }
    } = api.config;
    return this.serverWorkers[defaultWorker];
  }*/

  async initializeThreads() {
    this.serverWorkers = {};
    const promises = [];
    const {
      application: {
        serverWorkers: { initialWorkers }
      }
    } = api.config;
    for (const serverWorkerName of initialWorkers) {
      api.log(`Creating ServerWorker thread ${serverWorkerName} ...`, 'debug');

      // eslint-disable-next-line no-unused-vars
      const serverWorkerOnline = new Promise((resolve /*, reject*/) => {
        // eslint-disable-next-line security/detect-object-injection
        const serverWorker = new Worker(__filename, {
          workerData: {
            serverWorkerName
          }
        });

        serverWorker.on('error', (e) => {
          api.log(e);
        });

        serverWorker.on('exit', (exitCode) => {
          api.log(`ServerWorker exit code=${exitCode}`, 'warn');
        });

        serverWorker.on('message', (message) => {
          api.log('ServerWorker message', 'debug', message);
        });

        serverWorker.on('online', () => {
          api.log(`ServerWorker ${serverWorkerName} online`, 'debug');
          resolve(serverWorker);
        });

        // eslint-disable-next-line security/detect-object-injection
        this.serverWorkers[serverWorkerName] = serverWorker;
      });
      promises.push(serverWorkerOnline);
    }

    return Promise.all(promises);
  }

  /**
   * Worker event-handler.
   * Main entrypoint for *any* message coming through this Engine-as-a-ServerWorker.
   * @param {Buffer} message A Buffer containing a ProtoBuf object.
   * @returns {Promise<void>}
   */
  async onMessagePortMessage(message) {
    this.messageQueue.push(message);
  }

  consumeOneQueueMessage() {
    let message = this.messageQueue.shift();

    if (!message) {
      const {
        application: {
          serverWorkers: { idleQueueDelayTime }
        }
      } = api.config;
      setTimeout(this.consumeOneQueueMessage.bind(this), idleQueueDelayTime);
      return;
    }

    const clientId = message.clientId;

    // ServerNode Transmission Time (sntt), the time over the MessagePort for this message
    const sntt = Date.now() - message.receivedAt;

    message = api.spec.fromBuffer(message.raw);

    api.log(`clientId=${clientId} sntt=${sntt}ms message=`, 'debug', message);

    const {
      MessageType: { WELCOME, ADD_CONNECTION, REMOVE_CONNECTION }
    } = api.spec;

    // Perform an action appropriate for the MessageType
    switch (message.messageType) {
      case WELCOME:
        break;

      case ADD_CONNECTION:
        this.map.addConnection(message.connectionId, {});
        break;

      case REMOVE_CONNECTION:
        this.map.removeConnection(message.connectionId, {});
        break;

      default:
        break;
    }

    // Requeue this function to be called again
    setTimeout(this.consumeOneQueueMessage.bind(this), 0);
  }

  async onMessagePortClose() {
    api.log('Message port closed', 'warn');
  }

  async loadSpec() {
    const { Spec } = require('../../index');
    api.spec = new Spec();
    await api.spec.initialize();

    for (const messageType of Object.keys(api.spec.MessageType)) {
      const type = pascalCase(messageType);
      // eslint-disable-next-line no-prototype-builtins
      if (api.spec.types.hasOwnProperty(type)) {
        api.log(`MessageType '${messageType}' -> Type '${type}'`);
        // eslint-disable-next-line security/detect-object-injection
        api.spec.mapType(api.spec.MessageType[messageType], api.spec.types[type]);
      } else {
        api.log(`MessageType '${messageType}' has no corresponding Type`, 'warn');
      }
    }
  }

  async initializeRedis() {
    const { Redis } = require('../../index');
    api.redis = new Redis();
    await api.redis.initialize();
  }

  async loadScheduler() {
    if (api.config.scheduler.enabled === true) {
      const { Scheduler } = require('../../index');
      api.scheduler = new Scheduler();
      await api.scheduler.initialize();
    }
  }

  async initializeHooks() {
    const processesByPriority = Object.values(api.processes).sort((p1, p2) => {
      return p1.priority - p2.priority;
    });

    if (isMainThread) {
      // Connection hooks
      for (const process of processesByPriority) {
        for (const [serverType, server] of Object.entries(api.servers)) {
          if (process.hooks.indexOf('connection') !== -1) {
            // This process has connection hooks defined
            if (process.connectionTypes.indexOf(serverType)) {
              server.middleware.push(process);
              api.log(`${process.name} -> ${serverType}`);
            }
          }
        }
      }
    }

    // Execute initializer hooks
    for (const process of processesByPriority) {
      if (process.hooks.indexOf('initializer') !== -1) {
        api.log(`[${process.priority}] Initializing ${process.name}`);
        await process.initialize();
      }
    }
  }

  async start() {
    if (isMainThread) {
      // eslint-disable-next-line no-unused-vars
      return new Promise((resolve, reject) => {
        api.namespace.run(async () => {
          try {
            api.namespace.set('context', api.session);
            api.log('Engine starting');
            // eslint-disable-next-line no-unused-vars
            for (const server of Object.values(api.servers)) {
              await server.start();
            }
            api.log(`Ready in ${Math.floor(process.uptime() * 1000)} ms`);
            resolve();
          } catch (e) {
            reject(e);
          }
        });
      });
    } else {
      setTimeout(this.consumeOneQueueMessage.bind(this), 0);
      await this.map.start();
    }
  }

  async stop() {
    if (isMainThread) {
      // eslint-disable-next-line no-unused-vars
      return new Promise((resolve /*, reject*/) => {
        api.namespace.run(async () => {
          api.namespace.set('context', api.session);
          // eslint-disable-next-line no-unused-vars
          for (const server of Object.values(api.servers)) {
            await server.stop();
          }
          api.log('Engine stopping');
          resolve();
        });
      });
    } else {
      await this.map.stop();
    }
  }

  async shutdown() {
    if (isMainThread) {
      // eslint-disable-next-line no-unused-vars
      return new Promise((resolve /*, reject*/) => {
        api.namespace.run(async () => {
          api.namespace.set('context', api.session);
          // eslint-disable-next-line no-unused-vars
          for (const server of Object.values(api.servers)) {
            await server.stop();
          }
          api.log('Engine stopping');
          resolve();
        });
      });
    } else {
      //
    }
  }

  runInContext(fn, args) {
    return new Promise((resolve) => {
      return this.generateId().then((contextId) => {
        return api.namespace.run(async () => {
          api.namespace.set('context', contextId);
          resolve(fn(args));
        });
      });
    });
  }
}

module.exports = Engine;

if (isMainThread) {
  // Do nothing; Engine instantiation is handled by an externally-invoked command
} else {
  let api;
  (async () => {
    try {
      api = require('../..');
      api.engine = new Engine({
        messagePort: parentPort,
        data: workerData
      });
      await api.engine.initialize();
      await api.engine.start();
    } catch (e) {
      console.trace(e);
    }
  })();
}
