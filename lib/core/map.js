'use strict';

let api;

global.CANNON = require('cannon');

const path = require('path');
const BABYLON = require('babylonjs');
const { World } = require('ecsy');

// const { Object3D, Moving } = require('../../lib/component');
// const { Command } = require('../../lib/command');
// const { MovingSystem } = require('../../lib/system');
// const { Assemblage } = require('../../lib/assemblage');

const {
  // NullEngine,
  // Scene,
  // PointLight,
  // Vector3,
  // ArcRotateCamera,
  // OimoJSPlugin,
  // AmmoJSPlugin,
  CannonJSPlugin,
  // SceneLoader: {
  //   ImportMeshAsync
  // }
} = BABYLON;

const DEFAULT_FPS = 10;

module.exports = class Map {
  id = 0;
  serverWorker = null;
  engine = null;
  scene = null;
  camera = null;
  physics = null;
  world = null;
  rootMesh = null;
  lastTime = 0;
  players = {};
  assemblage = null;

  constructor(options = {}) {
    api = require('../..').api;

    this.serverWorker = options.serverWorker;
    this.locationId = options.locationId;
  }

  async initialize() {
    await this.initializeScene();
    await this.loadECS();
  }

  async initializeScene() {
    this.engine = new BABYLON.NullEngine({
      deterministicLockstep: true,
      lockstepMaxSteps: 4
    });
    this.scene = new BABYLON.Scene(this.engine);
    this.camera = new BABYLON.ArcRotateCamera(`Camera ${this.id}`, 0, 0.8, 1, BABYLON.Vector3.Zero(), this.scene);

    // Enable physics engine
    this.physics = new CannonJSPlugin();
    this.scene.enablePhysics(undefined, this.physics);

    this.world = new World(); // a whole new world
    this.rootMesh = BABYLON.MeshBuilder.CreateBox('box', { size: 1 }, this.scene);
  }

  async loadECS() {
    const componentPath = path.join(process.cwd(), 'lib', 'ecs', 'component');
    // eslint-disable-next-line security/detect-non-literal-require
    const Components = require(componentPath);
    api.log('Components:', 'debug');
    for (const [name, component] of Object.entries(Components).sort((a, b) => a[0].localeCompare(b[0]))) {
      api.log(` - ${name}`, 'debug');
      this.world.registerComponent(component);
    }

    const systemPath = path.join(process.cwd(), 'lib', 'ecs', 'system');
    const Systems = require(systemPath);
    api.log('Systems:', 'debug');
    for (const [name, system] of Object.entries(Systems).sort((a, b) => a[0].localeCompare(b[0]))) {
      api.log(` - ${name}`, 'debug');
      this.world.registerSystem(system);
    }

    const { Assemblage } = require(path.join('..', 'ecs', 'assemblage'));
    this.assemblage = new Assemblage(this.world);
  }

  async start() {
    api.log(`Starting ServerWorker ID=${this.locationId}`);

    this.world.execute(0, 0);

    let loopIterations = 0;
    this.lastTime = performance.now() / 1000;
    this.engine.runRenderLoop(() => {
      let time = performance.now() / 1000;
      let delta = time - this.lastTime;
      this.lastTime = time;

      // TODO: Limit rendering and updates to this.fps
      this.scene.render({
        updateCameras: false,
        ignoreAnimations: true
      });

      this.world.execute(delta, time);

      loopIterations++;
    });
  }

  async stop() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  addConnection(connectionId, metadata) {
    // eslint-disable-next-line no-prototype-builtins
    if (this.players.hasOwnProperty(connectionId)) {
      api.log(`Adding connection ${connectionId} failed; already exists`, 'error');
    } else {
      // eslint-disable-next-line security/detect-object-injection
      const { Player } = require('../..');
      // eslint-disable-next-line security/detect-object-injection
      this.players[connectionId] = new Player({
        map: this,
        connectionId,
        level: null,
        state: null,
        metadata
      });
      api.log(`Adding connection ${connectionId} succeeded`, 'info');

      api.log('Players:', 'debug', Object.keys(this.players));
    }
  }

  removeConnection(connectionId) {
    // eslint-disable-next-line no-prototype-builtins
    if (this.players.hasOwnProperty(connectionId)) {
      // eslint-disable-next-line security/detect-object-injection
      delete this.players[connectionId];
      api.log(`Removed connection ${connectionId}`, 'info');
      api.log('Players:', 'debug', Object.keys(this.players));
    } else {
      api.log(`Removing connection ${connectionId} failed; connection does not exist`, 'error');
    }
  }
};
