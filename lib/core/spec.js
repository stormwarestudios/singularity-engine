'use strict';

const path = require('path');
const Protobuf = require('protobufjs');

let api;

module.exports = class Spec {
  constructor() {
    api = require('../../index').api;
    this.typeMap = [];
  }

  /**
   * Map a typ-enum to its protobuf model.
   * @param {string} enumeration Type type specification enumeration.
   * @param {object} type The type specification model.
   */
  mapType(enumeration, type) {
    // eslint-disable-next-line security/detect-object-injection
    this.typeMap[enumeration] = type;
  }

  async initialize() {
    try {
      const protoPath = path.join(process.cwd(), api.config.application.spec);
      if (!protoPath) {
        api.log(`No protobuf spec file found at ${api.config.application.spec}; exiting...`, 'warn');
        return;
      }
      api.log(`Loading protobuf spec file from ${api.config.application.spec}`);
      const { nested: {
        singularity
      } } = await Protobuf.load(protoPath);
      this.spec = singularity;
      this.typeMap.length = Object.keys(this.spec.MessageType).length; // optimization
    } catch (e) {
      api.log(e);
    }
  }

  get MessageType() {
    return this.spec.MessageType;
  }

  get types() {
    return this.spec;
  }

  /**
   * Convert a JSON message to its binary-data protobuf format.
   * JSON -> Protobuf
   * @param payload
   * @returns {Buffer}
   */
  toBuffer(payload) {
    const type = this.typeMap[payload.messageType];
    const errMsg = type.verify(payload);
    if (errMsg) throw Error(errMsg);
    const message = type.fromObject(payload);
    return type.encode(message).finish();
  }

  /**
   * Convert a binary-data protobuf message to its JSON format.
   * Protobuf -> JSON
   * @param {Buffer} data
   * @returns {object}
   */
  fromBuffer(data) {
    const buffer = Buffer.from(data);
    const base = this.spec.Base.decode(buffer);
    return this.typeMap[base.messageType].decode(buffer);
  }
};
