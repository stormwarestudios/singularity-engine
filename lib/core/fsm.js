'use strict';

let api;

module.exports = class Fsm {
  constructor() {
    api = require('../../index').api;
    this.states = {};
    this.previousState = null;
    this.currentState = null;
  }

  registerState(key, state) {
    const { GameState } = api;
    if (state instanceof GameState) {
      // eslint-disable-next-line security/detect-object-injection
      this.states[key] = state;
      state.setFsm(this);
    } else {
      console.log(`FSM state '${key}' is not a GameState`);
    }
  }

  async transition(newState) {
    // eslint-disable-next-line no-prototype-builtins
    if (!this.states.hasOwnProperty(newState)) {
      console.error(`Requested state '${newState}' is not a GameState`);
      return;
    }

    if (this.previousState) {
      console.groupEnd();
    }

    if (this.currentState) {
      await this.currentState._onExit();
      console.log(`${this.previousState} -> ${newState}`);
    } else {
      console.log(`(null) -> ${newState}`);
    }

    this.previousState = newState;

    this.currentState = this.states[newState];

    console.group(`${newState.toUpperCase()} STATE`);

    await this.currentState._onEnter();
  }
};
