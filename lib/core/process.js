'use strict';

let api;

module.exports = class Process {
  constructor() {
    api = require('../../index').api;

    /**
     * Process name; must be unique in the system.
     * @type {string}
     */
    this.name = 'a process';

    /**
     * A friendly description for your process.
     * @type {string}
     */
    this.description = 'a process description';

    /**
     * A cron-like schedule on which your process will run if scheduled, or null if it should never be scheduled.
     * @type {string|null}
     */
    this.schedule = null;

    /**
     * Startup and shutdown priority; executed in ascending order on startup, and descending order on shutdown.
     * @type {number}
     */
    this.priority = 1000;

    /**
     * Hook-types to which this process will be configured to respond.
     * @type {Array} Zero or more of connection, command or initializer
     */
    this.hooks = [];

    /**
     * Connection-types to which this process will be configured to respond.
     * @type {Array} Zero or more of web, websocket, grpc, task or amqp
     */
    this.connectionTypes = [];
    this.middleware = [];
  }

  /**
   * Called once on startup, in priority order with other processes.
   * @returns {Promise<void>}
   */
  async initialize() {
    api.log(`[${this.priority}] Process ${this.name} initializing`, 'debug');
  }

  /**
   * Called as a middleware function, either immediately before exec(), or on connection
   * @param data
   * @returns {Promise<void>}
   */
  async pre(data) {
    // My preprocess hook code goes here
  }

  /**
   * Called as the main execution for this process.
   * @param {object} data
   * @returns {Promise<void>}
   */
  async exec(data) {
    // My logic goes here
  }

  /**
   * Called as a middleware function, either immediately after exec(), or on disconnection
   * @param data
   * @returns {Promise<void>}
   */
  async post(data) {}

  /**
   * Called once on shutdown, in reverse priority order with other processes.
   * @returns {Promise<void>}
   */
  async shutdown() {}
};
