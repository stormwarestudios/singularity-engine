'use strict';

// https://www.npmjs.com/package/cron-parser

const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const cronstrue = require('cronstrue');
const parser = require('cron-parser');

let api;

/**
 * Database util / helper class
 */
module.exports = class Scheduler {
  constructor() {
    api = require('../../index').api;
    this.db = null;
  }
  async onError(error) {
    api.log(error.message, 'error', error);
  }

  async initialize() {
    const {
      config: {
        scheduler: { options }
      }
    } = api;

    const PgBoss = require('pg-boss');
    this.boss = new PgBoss(options);
    this.boss.on('error', this.onError.bind(this));
  }

  async onJobComplete(job) {
    api.log(`Job completed`, 'debug', job);
  }

  async executeJob(job) {
    try {
      const { id, name, data } = job;
      // eslint-disable-next-line security/detect-object-injection
      const process = api.processes[name];
      await api.engine.runInContext(async () => {
        try {
          api.log(`Starting job id=${id} name=${name}`, 'debug');
          const start = Date.now();
          await process.exec({
            params: data
          });
          const time = Date.now() - start;
          api.log(`Finished job id=${id} name=${name} (${time} ms)`, 'debug');
        } catch (e) {
          api.log(e);
        }

        if (process.schedule !== null) {
          const next = parser
            .parseExpression(process.schedule)
            .next()
            .toString();
          // api.log(`Task '${process.name}' scheduled to run again at ${new Date(next).toISOString()}`);
          await this.enqueueAt(next, name, {}, 1000);
        }
      });
    } catch (e) {
      api.log(e);
    }
  }

  /**
   * List all processes which can be scheduled as tasks.
   * @returns {[]} An array of process names
   */
  listTaskProcessNames() {
    const tasks = [];

    for (const [name, process] of Object.entries(api.processes)) {
      if (process.connectionTypes.indexOf('task') !== -1) {
        tasks.push(name);
      } else {
        if (process.schedule !== null) {
          api.log(`Process '${name}' does not include connectionType of 'task' but has a schedule defined`, 'notice');
        }
      }
    }

    return tasks;
  }

  /**
   * Connect to the task scheduling system and start monitoring for tasks to be executed.
   * @returns {Promise<void>}
   */
  async start() {
    api.log(`Starting the scheduler`);
    await this.boss.start();

    if (!isMainThread) return;

    const processNames = this.listTaskProcessNames();

    const promises = [];

    for (const processName of processNames) {
      try {
        api.log(`Subscribing task '${processName}'`);
        promises.push(this.boss.subscribe(processName, {}, this.executeJob.bind(this)));
        // await this.boss.onComplete(this.onJobComplete.bind(this));
      } catch (e) {
        api.log(e);
      }
    }

    return Promise.all(promises)
      .then(async () => {
        const promises = [];
        for (const processName of processNames) {
          try {
            const process = api.processes[processName];
            if (process.schedule !== null) {
              const next = parser
                .parseExpression(process.schedule)
                .next()
                .toString();
              api.log(`Task '${process.name}' scheduled to run at ${next}`, 'debug');
              await this.enqueueAt(next, processName, {}, {});
            }
          } catch (e) {
            api.log(e);
          }
        }
        return Promise.all(promises);
      })
      .catch((e) => {
        api.log(e);
      });
  }

  /**
   * Stop monitoring for tasks.
   * @returns {Promise<void>}
   */
  async stop() {
    api.log(`Stopping the scheduler`);
    await this.boss.stop();
  }

  /**
   * Schedule a task to be executed immediately.
   * @param {string} name Name of the process to be executed as a task.
   * @param {object} data Options sent to the job at execution time.
   * @param {object} options Parameters passed directly to the underlying task scheduling system.
   * @returns {Promise<void>}
   */
  async enqueue(name, data, options) {
    const id = await this.boss.publish(name, data, options);
    if (!id) return;
    api.log(`Scheduled job id=${id} name=${name} to run immediately`, 'debug');
  }

  /**
   * Schedule a task to be executed on a specific date and time.
   * @param {number} timestamp The date and time at which the task will be executed.
   * @param {string} name Name of the process to be executed as a task.
   * @param {object} data Parameters passed directly to the underlying task scheduling system.
   * @param {object} options Parameters passed directly to the underlying task scheduling system.
   */
  async enqueueAt(timestamp, name, data, options) {
    try {
      const isoString = new Date(timestamp).toISOString();
      const id = await this.boss.publishAfter(name, data, options, isoString);
      api.log(`Scheduled job id=${id} name=${name} to run at ${isoString}`, 'debug');
    } catch (e) {
      api.log(e);
    }
  }

  /**
   * Schedule a task to be executed in a number of milliseconds from now.
   * @param {number} delay The delay from now after which the task will execute, in milliseconds.
   * @param {string} taskName Name of the process to be executed as a task.
   * @param {object} taskParameters Parameters passed directly to the underlying task scheduling system.
   * @param {number} priority Priority of the task; higher is more priority.
   */
  async enqueueIn(delay, taskName, taskParameters, priority) {
    api.log(`Scheduled job id=${id} name=${name} to immediately`, 'debug');
  }
};
