'use strict';

let api;

module.exports = class Player {
  constructor(options) {
    api = require('../..').api;

    const { map, connectionId, level, state, metadata } = options;

    Object.assign(this, {
      map, connectionId, level, state, metadata
    });

    console.log(api);

    const { Fsm } = api;
    this.fsm = new Fsm();


    for (const [name, GameState] of Object.entries(api.fsm)) {
      this.fsm.registerState(name, new GameState(this));
    }
  }

  send(data) {}
};
