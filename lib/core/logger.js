'use strict';

const ip = require('ip');

const {
  Worker, isMainThread, parentPort, workerData
// eslint-disable-next-line node/no-unsupported-features/node-builtins
} = require('worker_threads');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf, colorize, splat, align } = format;

const errorStackFormat = format(info => {
  if (info instanceof Error) {
    return Object.assign({}, info, {
      stack: info.stack,
      message: info.message
    });
  }
  return info;
});

const myFormat = printf(({ level, message, label, timestamp, serverIp, context, object = null }) => {
  const { threadId } = Worker;
  const { serverWorkerName } = workerData || { serverWorkerName: 'main' };

  if (message instanceof Error) {
    return `${serverIp} @ ${timestamp} [${serverWorkerName}] [${context}] - ${level}: ${message}\n` + message.stack;
  } else {
    return `${serverIp} @ ${timestamp} [${serverWorkerName}] [${context}] - ${level}: ${message} ${object ? JSON.stringify(object) : ''}`;
  }
});

const logger = createLogger({
  format: combine(timestamp(), colorize(), splat(), errorStackFormat(), myFormat),
  transports: [new transports.Console({ level: 'debug' })]
});

module.exports = class Logger {
  static log(message, level = 'info', object = '') {
    const { api } = require('../../index');
    const serverIp = ip.address();
    if (message instanceof Error) {
      level = 'error';
    }

    logger.log({
      level,
      message,
      serverIp,
      context: api.namespace.get('context') || api.session,
      object
    });
  }
};
