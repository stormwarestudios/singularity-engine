'use strict';

const { EventEmitter } = require('events');

let api;

module.exports = class Server extends EventEmitter {
  constructor() {
    api = require('../../index').api;

    super();
    this.type = 'default';
    this.middleware = [];
  }

  async initialize() {
    api.log(`Initializing ${this.type} server`);
  }
  async start() {
    api.log(`Starting ${this.type} server`);
  }
  async stop() {
    api.log(`Stopping ${this.type} server`);
  }
  async shutdown() {
    api.log(`Shutting down ${this.type} server`);
  }
};
