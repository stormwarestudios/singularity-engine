'use strict';

const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');

const fs = require('fs-extra');
const path = require('path');
const { exec } = require('child_process');
const Sequelize = require('sequelize');

let api;

/**
 * Database util / helper class
 */
module.exports = class Database {
  constructor() {
    api = require('../../index').api;
    this.db = null;
    this.Sequelize = Sequelize;
  }

  async initialize() {
    const cfg = api.config.database;

    if (cfg.useCLS === true) {
      const cls = require('cls-hooked');
      const namespace = cls.createNamespace('database');
      Sequelize.useCLS(namespace);
    }

    api.log(`Connecting to ${cfg.options.dialect}://${cfg.username}@${cfg.options.host}:${cfg.options.port}/${cfg.db}`, 'debug');

    // Patch the application_name
    const { serverWorkerName } = workerData || { serverWorkerName: 'main' };
    cfg.options.dialectOptions.application_name = `${cfg.options.dialectOptions.application_name} (${serverWorkerName})`;
    cfg.options.logging = this.log.bind(this);

    this.db = new Sequelize(cfg.db, cfg.username, cfg.password, cfg.options);

    api.log(`Authenticating...`, 'debug');
    await this.db.authenticate();
    api.log(`Loading models...`, 'debug');
    await this.loadModels();
  }

  log(msg, benchmark) {
    api.log(`${msg} (${benchmark} ms)`, 'debug');
  }

  /**
   * Run db migration 'up' job.
   * @returns {Promise<any>}
   */
  async migrateUp() {
    return new Promise((resolve, reject) => {
      exec(`npx sequelize db:migrate`, (error, stdout, stderr) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }

  async loadModels() {
    const db = {};

    const modelsDir = process.cwd() + '/db/models/';
    const pathExists = await fs.pathExists(modelsDir);
    if (pathExists === false) {
      return;
    }

    fs.readdirSync(modelsDir)
      .filter((file) => file.indexOf('.') !== 0 && file !== 'index.js' && file.slice(-3) === '.js')
      .forEach((file) => {
        const modelFilePath = path.join(modelsDir, file);
        const model = require(modelFilePath)(this.db, Sequelize.DataTypes);
        api.log(`  Loading model ${model.name} ...`, 'debug');
        db[model.name] = model;
      });

    Object.keys(db).forEach((modelName) => {
      if (db[modelName].associate) {
        db[modelName].associate(db);
      } else {
        api.log(`Model ${modelName} has no associate function`, 'warn');
      }
    });

    this.db.models = db;
    api.log(`Loaded models ${Object.keys(this.db.models).join(', ')}`, 'debug');
  }

  /**
   * Establish a Sequelize one-to-one relation.
   * A.belongsTo(B)
   * B.hasOne(A)
   *
   * @param {Object} models An object containing all database models.
   * @param {String} srcTable The source table
   * @param {String} srcColumn The source table column
   * @param {String} destTable The target table
   * @param {String} destColumn The target table column
   * @param {String} keyName The foreign key name
   */
  static oneToOne(models, srcTable, srcColumn, destTable, destColumn, keyName) {
    api.log(`1:1 ${srcTable}.${srcColumn} -> ${destTable}.${destColumn}`);
    let src = models[srcTable];
    let dest = models[destTable];
    src.belongsTo(dest);
    dest.hasOne(src);
  }

  /**
   * Establish a Sequelize one-to-many relation.
   * A.belongsTo(B)
   * B.hasMany(A)
   *
   * @param {Object} models An object containing all database models.
   * @param {String} srcTable The source table
   * @param {String} srcColumn The source table column
   * @param {String} destTable The target table
   * @param {String} destColumn The target table column
   * @param {String} keyName The foreign key name
   */
  static oneToMany(models, srcTable, srcColumn, destTable, destColumn, keyName) {
    api.log(`1:n ${srcTable}.${srcColumn} -> ${destTable}.${destColumn}`);
    let src = models[srcTable];
    let dest = models[destTable];
    src.belongsTo(dest, { foreignKey: srcColumn, targetKey: destColumn });
    dest.hasMany(src, { foreignKey: srcColumn, sourceKey: destColumn });
  }

  /**
   * Establish a Sequelize many-to-many relation.
   * A.belongsToMany(C, { through: B });
   * C.belongsToMany(A, { through: B });
   *
   * @param {Object} models An object containing all database models.
   * @param {Object} joinTable An object defining the join table's parameters.
   * @param {String} joinTable.joinTable The table forming the m:n relation table.
   * @param {String} joinTable.joinTableLeftColumn The 'm' side of the m:n relation.
   * @param {String} joinTable.joinTableRightColumn The 'n' side of the m:n relation.
   * @param {String} joinTable.leftTable The 'm' table name.
   * @param {String} joinTable.leftTableColumn The 'm' table's join column name.
   * @param {String} joinTable.rightTable The 'n' table name.
   * @param {String} joinTable.rightTableColumn The 'n' table's join column name.
   */
  static manyToMany(models, joinTable) {
    api.log(
      `m:n ${joinTable.joinTable}.${joinTable.joinTableLeftColumn} -> ${joinTable.leftTable}.${joinTable.leftTableColumn}, ${joinTable.joinTable}.${joinTable.joinTableRightColumn} -> ${joinTable.rightTable}.${joinTable.rightTableColumn}`
    );
    let left = models[joinTable.leftTable];
    let right = models[joinTable.rightTable];
    let join = models[joinTable.joinTable];
    let leftCriteria = { through: join, foreignKey: joinTable.joinTableLeftColumn };
    let rightCriteria = { through: join, foreignKey: joinTable.joinTableRightColumn };
    if (joinTable.leftTable === joinTable.rightTable) {
      leftCriteria.as = joinTable.joinTable;
      rightCriteria.as = joinTable.joinTable;
    }
    left.belongsToMany(right, leftCriteria);
    right.belongsToMany(left, rightCriteria);
    if (joinTable.leftTable !== joinTable.rightTable) {
      right.hasMany(join);
      join.belongsTo(left);
      left.hasMany(join);
      join.belongsTo(right);
    }
  }
};
