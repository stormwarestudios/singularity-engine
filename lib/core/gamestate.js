'use strict';

let api;

const PubSub = require('pubsub-js');

module.exports = class GameState {
  constructor(player) {
    api = require('../..').api;

    this.player = player;
    this.subscriptions = {};
    this.fsm = null;
    //console.log(`${this.constructor.name}.ctor()`);
  }

  setFsm(fsm) {
    this.fsm = fsm;
  }

  async handle(subState) {
    // eslint-disable-next-line no-prototype-builtins
    if (this.hasOwnProperty(subState)) {
      this[subState].apply(this);
    } else {
      api.log(`Unable to handle '${this.constructor.name}.${subState}()'`);
    }
  }

  async _onEnter() {
    api.log(`${this.constructor.name}::_onEnter()`);
  }

  async _onExit() {
    api.log(`${this.constructor.name}::_onExit()`);
    this.unsubscribeAll();
    await this.handle('cleanup');
  }

  async cleanup() {
    api.log(`${this.constructor.name}::cleanup()`);
  }

  async transition(newState) {
    this.fsm.transition(newState);
  }

  subscribe(topic, cb) {
    this.subscriptions[topic] = PubSub.subscribe(topic, cb);
    //console.log(`${this.constructor.name}: subscribed to topic '${topic}'`);
  }

  unsubscribe(topic) {
    if (this.subscriptions.hasOwnProperty(topic)) {
      PubSub.unsubscribe(this.subscriptions[topic]);
      delete this.subscriptions[topic];
      //console.log(`${this.constructor.name}: unsubscribed from topic '${topic}'`);
    }
  }

  unsubscribeAll() {
    for (const topic of Object.keys(this.subscriptions)) {
      this.unsubscribe(topic)
    }
  }
};
