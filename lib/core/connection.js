'use strict';

/**
 * Given a Cookie HTTP header string, parse it and return an object containing the entries as key/value pairs.
 * @param {string} cookie
 */
function parseCookies(cookie) {
  const rx = /([^;=\s]*)=([^;]*)/g;
  const obj = { };
  // eslint-disable-next-line no-cond-assign
  for ( let m ; m = rx.exec(cookie) ; )
    obj[ m[1] ] = decodeURIComponent( m[2] );
  return obj;
}

const Cookies = require('cookies');
let api;

module.exports = class Connection {
  constructor(id, options) {
    api = require('../..').api;

    this.id = id;
    this.connectedAt = options.connectedAt;
    this.fingerprint = null;
    this.messageId = null;
    this.params = options.params;
    this.rawConnection = options.rawConnection;
    this.remoteIP = options.remoteIP;
    this.remotePort = null;
    this.type = options.type;
    //this.cookies = this.type === 'web' ? cookie.parse(options.rawConnection.req.headers.cookie || '') : {};
    this.serverWorker = null;
    this.isAuthenticated = false;
    this.metadata = null;

    const { req, res } = this.rawConnection;
    if (req && res) {
      this.cookies = new Cookies(req, res, {
        secure: false
      });
    } else {
      this.cookies = parseCookies(this.rawConnection.headers.cookie);
    }

    if (this.type === 'websocket') {
      this.rawConnection.on('data', this.onData.bind(this));
      this.rawConnection.on('end', this.onEnd.bind(this));
    }
  }

  onData(data) {
    if (this.isAuthenticated === true) {
      this.postToServerWorker(data);
    } else {
      api.log(`Got message from unauthenticated client ${this.id}`, 'warn', data.toString());
      this.disconnect({
        messageType: api.spec.MessageType.NOT_AUTHORIZED,
        message: 'you are not authorized to send messages and will be disconnected'
      });
    }
  }

  onEnd() {}

  disconnect(data = undefined, options = { reconnect: false }) {
    this.rawConnection.end(data ? api.spec.toBuffer(data) : data, options);
  }

  /**
   * Send a message to the websocket client on the other end.
   * @param {object} data
   * @returns {Promise<void>}
   */
  async send(data) {
    this.rawConnection.write(api.spec.toBuffer(data));
  }

  /**
   * Post a message via MessagePort to this connection's ServerWorker thread.
   */
  postToServerWorker(raw) {
    this.serverWorker.postMessage({
      receivedAt: Date.now(),
      clientId: this.id,
      raw
    });
  }

  moveToServerWorker(name) {
    const oldServerWorker = this.serverWorker;
    if (oldServerWorker) {
      this.postToServerWorker(api.spec.toBuffer({
        messageType: api.spec.MessageType.REMOVE_CONNECTION,
        connectionId: this.id,
        ipAddress: this.remoteIP,
        port: this.remotePort
      }));
    }

    // eslint-disable-next-line security/detect-object-injection
    const newServerWorker = api.engine.serverWorkers[name];
    if (newServerWorker) {
      this.serverWorker = newServerWorker;

      this.postToServerWorker(api.spec.toBuffer({
        messageType: api.spec.MessageType.ADD_CONNECTION,
        connectionId: this.id,
        ipAddress: this.remoteIP,
        port: this.remotePort
      }));
    }
  }
};
