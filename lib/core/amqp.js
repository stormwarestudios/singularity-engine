'use strict';

const amqp = require('amqp-connection-manager');

let api;

function retryStrategy(times) {
  return Math.min(times * 50, 2000);
}

module.exports = class Redis {
  constructor() {
    api = require('../../index').api;

    this.subscriptions = {};
  }

  async initialize() {
    const {
      config: {
        application: { cluster },
        amqp
      },
      session
    } = api;
  }

  async onClusterMessage(channel, message) {
    api.log(`[CLUSTER] channel=${channel} message=${message}`, 'debug');
  }

  async onSessionMessage(channel, message) {
    api.log(`[SESSION] channel=${channel} message=${message}`, 'debug');
  }

  async onMessageBuffer(channel, data) {
    try {
      const message = api.spec.fromBuffer(data);
      api.log(`[BUFFER] channel='${channel}'`, 'debug', message);
    } catch (e) {
      api.log(e);
    }
  }

  async subscribe(channel, fn) {
    // eslint-disable-next-line promise/avoid-new
    return new Promise((resolve, reject) => {
      this.sub.subscribe(channel, (err, count) => {
        if (err) {
          return reject(err);
        }
        // eslint-disable-next-line security/detect-object-injection
        this.subscriptions[channel] = fn;
        resolve(count);
      });
    });
  }

  async load() {}

  async save() {}

  async exec(which, command, args) {
    if (which !== 'pub' && which !== 'sub' && which !== 'cache') {
      throw new Error(`invalid Redis client ${which}`);
    }
    // eslint-disable-next-line security/detect-object-injection
    return this[which][command](...args);
  }

  async publish(channel, message) {
    try {
      this.pub.publish(channel, message);
    } catch (e) {
      api.log(e);
    }
  }
};
